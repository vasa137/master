<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obrazovanje extends Model
{
    protected $table = 'obrazovanje';
    protected $fillable = ['id_obrazovna_ustanova', 'id_stepen_studija', 'zvanje', 'datum'];

    protected $appends = ['obrazovnaUstanova', 'stepenStudija'];

    private $obrazovnaUstanova;
    private $stepenStudija;


    public function getObrazovnaUstanovaAttribute()
    {
        return $this->obrazovnaUstanova;
    }

    public function setObrazovnaUstanovaAttribute($obrazovnaUstanova)
    {
        $this->obrazovnaUstanova = $obrazovnaUstanova;
    }

    public function getStepenStudijaAttribute()
    {
        return $this->stepenStudija;
    }

    public function setStepenStudijaAttribute($stepenStudija)
    {
        $this->stepenStudija = $stepenStudija;
    }

    public static function dohvatiSaId($id){
        return Obrazovanje::where('id', $id)->first();
    }

    public function napuni($id_obrazovna_ustanova, $id_stepen_studija, $zvanje, $datum){
        $this->id_obrazovna_ustanova = $id_obrazovna_ustanova;
        $this->id_stepen_studija = $id_stepen_studija;
        $this->zvanje = $zvanje;
        $this->datum = $datum;

        $this->save();
    }

    public function obrisi(){
        $this->delete();
    }
}
