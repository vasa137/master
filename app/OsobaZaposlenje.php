<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsobaZaposlenje extends Model
{
    protected $table = 'osoba_zaposlenje';
    protected $fillable = ['id_osoba', 'id_zaposlenje'];

    public static function dohvatiZaOsobu($id_osoba){
        return OsobaZaposlenje::where('id_osoba', $id_osoba)->get();
    }

    public function napuni($id_osoba, $id_zaposlenje){
        $this->id_osoba = $id_osoba;
        $this->id_zaposlenje = $id_zaposlenje;

        $this->save();
    }

    public function obrisi(){
        OsobaZaposlenje::where('id_osoba', $this->id_osoba)->where('id_zaposlenje', $this->id_zaposlenje)->delete();
    }
}
