<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObrazovnaUstanova extends Model
{
    protected $table = 'obrazovna_ustanova';
    protected $fillable = ['naziv', 'opis', 'sakriven'];

    protected $appends = ['broj_osoba'];

    private $broj_osoba;

    public function getBrojOsobaAttribute()
    {
        return $this->broj_osoba;
    }

    public function setBrojOsobaAttribute($broj_osoba)
    {
        $this->broj_osoba = $broj_osoba;
    }

    public static function dohvatiSaId($id){
        return ObrazovnaUstanova::where('id', $id)->first();
    }

    public static function dohvatiMedjuAktivnim($id){
        return ObrazovnaUstanova::where('id', $id)->where('sakriven', 0)->first();
    }

    public static function dohvatiSveAktivne(){
        return ObrazovnaUstanova::where('sakriven', 0)->orderBy('naziv', 'asc')->get();
    }

    public static function dohvatiSveObrisane(){
        return ObrazovnaUstanova::where('sakriven', 1)->orderBy('naziv', 'asc')->get();
    }

    public function napuni($naziv, $opis){
        $this->naziv = $naziv;
        $this->opis = $opis;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }
}
