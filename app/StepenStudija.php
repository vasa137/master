<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StepenStudija extends Model
{
    protected $table = 'stepen_studija';
    protected $fillable = ['naziv', 'opis', 'sakriven'];

    public static function dohvatiSaId($id){
        return StepenStudija::where('id', $id)->first();
    }

    public static function dohvatiMedjuAktivnim($id){
        return StepenStudija::where('id', $id)->where('sakriven', 0)->first();
    }

    public static function dohvatiSveAktivne(){
        return StepenStudija::where('sakriven', 0)->get();
    }
}
