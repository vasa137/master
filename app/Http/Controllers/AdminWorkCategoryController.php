<?php

namespace App\Http\Controllers;

use App\KategorijaRada;
use App\OsobaProjekat;
use App\OsobaReferenca;
use App\Projekat;
use App\Referenca;
use Illuminate\Http\Request;
use Redirect;

class AdminWorkCategoryController extends Controller
{
    private function fillWorkCategoryInfo($kategorija_rada){
        $projekti = Projekat::dohvatiSvePoslate();

        $broj_radova = 0;

        foreach($projekti as $projekat){
            // RUKOVODILAC
            $osobaReference = OsobaReferenca::dohvatiZaOsobu($projekat->id_osoba);

            foreach($osobaReference as $osobaReferenca){
                $referenca = Referenca::dohvatiSaId($osobaReferenca->id_referenca);

                if($referenca->id_kategorija_rada == $kategorija_rada->id){
                    $broj_radova++;
                }
            }

            // UCESNICI
            $osobeProjekat = OsobaProjekat::dohvatiZaProjekat($projekat->id);

            foreach($osobeProjekat as $osobaProjekat){
                $osobaReference = OsobaReferenca::dohvatiZaOsobu($osobaProjekat->id_osoba);

                foreach($osobaReference as $osobaReferenca){
                    $referenca = Referenca::dohvatiSaId($osobaReferenca->id_referenca);

                    if($referenca->id_kategorija_rada == $kategorija_rada->id){
                        $broj_radova++;
                    }
                }
            }
        }

        $kategorija_rada->broj_radova = $broj_radova;
    }

    public function workCategory($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        if(!$izmena){
            return view('admin.adminKategorijaRada', compact('izmena'));
        } else{
            $kategorija_rada = KategorijaRada::dohvatiSaId($id);

            if($kategorija_rada == null){
                abort(404);
            }

            $this->fillWorkCategoryInfo($kategorija_rada);

            return view('admin.adminKategorijaRada', compact('izmena', 'kategorija_rada'));
        }
    }

    public function workCategories(){
        $aktivnihKategorijaRadova = KategorijaRada::dohvatiSveAktivne();
        $obrisanihKategorijaRadova = KategorijaRada::dohvatiSveObrisane();

        foreach($aktivnihKategorijaRadova as $kategorija_rada){
            $this->fillWorkCategoryInfo($kategorija_rada);
        }

        foreach($obrisanihKategorijaRadova as $kategorija_rada){
            $this->fillWorkCategoryInfo($kategorija_rada);
        }

        return view('admin.adminKategorijeRadova', compact('aktivnihKategorijaRadova', 'obrisanihKategorijaRadova'));
    }

    public function saveWorkCategory($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];

        $toFill = true;

        if($izmena){
            $kategorija_rada = KategorijaRada::dohvatiSaId($id);

            if($kategorija_rada->naziv == $naziv && $kategorija_rada->opis == $opis ){
                $toFill = false;
            }

        } else{
            $kategorija_rada = new KategorijaRada();
        }

        if($toFill) {
            $kategorija_rada->napuni($naziv, $opis);
        }

        return redirect('/admin/kategorija-rada/' . $kategorija_rada->id);
    }

    public function deleteWorkCategory($id){
        $kategorija_rada = KategorijaRada::dohvatiSaId($id);

        $kategorija_rada->obrisi();

        return Redirect::back();
    }

    public function restaurateWorkCategory($id){
        $kategorija_rada = KategorijaRada::dohvatiSaId($id);

        $kategorija_rada->restauriraj();

        return Redirect::back();
    }
}
