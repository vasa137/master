<?php

namespace App\Http\Controllers;

use App\NaucnaUstanova;
use App\OblastProjekat;
use App\Osoba;
use App\OsobaProjekat;
use App\OsobaZaposlenje;
use App\Projekat;
use App\Zaposlenje;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function home(){
        $totalWeekProjects = 0;
        $totalWeekBudget = 0;

        $datum =  date('Y-m-d', time());
        $datum = date('Y-m-d', strtotime($datum . ' -6 day'));

        $datum1 = $datum;

        $brojeviProjekata = [];
        $iznosi = [];
        $datumi = [];


        for($i = 0; $i < 7; $i++){
            $datumi[] = date('d.m.Y.', strtotime($datum));

            $brojeviProjekata[] = Projekat::dohvatiBrojPorudzbinaZaDatum($datum);
            $iznosi[] = Projekat::dohvatiUkupanIznosZaDatum($datum);

            $totalWeekProjects += $brojeviProjekata[$i];
            $totalWeekBudget += $iznosi[$i];

            $datum =  date('Y-m-d', strtotime($datum . ' +1 day'));
        }

        $datum2 = $datum;

        $infoIznosi = Projekat::dohvatiIznoseZaDatume($datum1, $datum2);

        $averageWeekBudget = $infoIznosi->prosek;
        $maxWeekBudget = $infoIznosi->maksimalna;

        $projekti = Projekat::dohvatiSvePoslate();

        return view('admin.adminWelcome', compact('projekti', 'datumi', 'brojeviProjekata', 'iznosi', 'totalWeekProjects', 'totalWeekBudget', 'averageWeekBudget', 'maxWeekBudget'));
    }

    public function projects(){
        $projekti = Projekat::dohvatiSvePoslate();

        $brojProjekata = 0;

        $totalBudget = 0;

        $totalMembers = 0;

        foreach($projekti as $projekat){
            $osoba = Osoba::dohvatiSaId($projekat->id_osoba);

            $osobaZaposlenja = OsobaZaposlenje::dohvatiZaOsobu($osoba->id);

            foreach($osobaZaposlenja as $osobaZaposlenje){
                $zaposlenje = Zaposlenje::dohvatiSaId($osobaZaposlenje->id_zaposlenje);

                if($zaposlenje->datum_kraja == null){
                    $osoba->trenutnaNaucnaUstanova = NaucnaUstanova::dohvatiSaId($zaposlenje->id_naucna_ustanova);
                    break;
                }
            }

            $projekat->osobe = [$osoba];

            $projekat->oblastProjekat = OblastProjekat::dohvatiSaId($projekat->id_oblast_projekat);

            $brojProjekata++;

            $totalBudget += $projekat->budzet;

            $totalMembers += 1 + count(OsobaProjekat::dohvatiZaProjekat($projekat->id));
        }

        return view('admin.adminProjekti', compact('projekti', 'brojProjekata', 'totalBudget', 'totalMembers'));
    }



}
