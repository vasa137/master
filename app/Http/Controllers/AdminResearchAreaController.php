<?php

namespace App\Http\Controllers;

use App\OblastIstrazivanja;
use App\Osoba;
use App\OsobaOblastIstrazivanja;
use App\OsobaProjekat;
use App\Projekat;
use Illuminate\Http\Request;
use Redirect;
class AdminResearchAreaController extends Controller
{
    private function fillResearchAreaInfo($oblast_istrazivanja){
        $projekti = Projekat::dohvatiSvePoslate();
        
        $broj_osoba = 0;

        foreach($projekti as $projekat){
            // RUKOVODILAC
            $osobaOblastiIstrazivanja = OsobaOblastIstrazivanja::dohvatiZaOsobu($projekat->id_osoba);

            foreach($osobaOblastiIstrazivanja as $osobaOblastIstrazivanja){
                if($osobaOblastIstrazivanja->id_oblast_istrazivanja == $oblast_istrazivanja->id){
                    $broj_osoba++;
                    break;
                }
            }

            // UCESNICI
            $osobeProjekat = OsobaProjekat::dohvatiZaProjekat($projekat->id);

            foreach($osobeProjekat as $osobaProjekat){
                $osobaOblastiIstrazivanja = OsobaOblastIstrazivanja::dohvatiZaOsobu($osobaProjekat->id_osoba);

                foreach($osobaOblastiIstrazivanja as $osobaOblastIstrazivanja){
                    if($osobaOblastIstrazivanja->id_oblast_istrazivanja == $oblast_istrazivanja->id){
                        $broj_osoba++;
                        break;
                    }
                }
            }
        }

        $oblast_istrazivanja->broj_osoba = $broj_osoba;
    }

    public function researchArea($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        if(!$izmena){
            return view('admin.adminOblastIstrazivanja', compact('izmena'));
        } else{
            $oblast_istrazivanja = OblastIstrazivanja::dohvatiSaId($id);

            if($oblast_istrazivanja == null){
                abort(404);
            }

            $this->fillResearchAreaInfo($oblast_istrazivanja);

            return view('admin.adminOblastIstrazivanja', compact('izmena', 'oblast_istrazivanja'));
        }
    }

    public function researchAreas(){
        $aktivnihOblastiIstrazivanja = OblastIstrazivanja::dohvatiSveAktivne();
        $obrisanihOblastiIstrazivanja = OblastIstrazivanja::dohvatiSveObrisane();

        foreach($aktivnihOblastiIstrazivanja as $oblast_istrazivanja){
            $this->fillResearchAreaInfo($oblast_istrazivanja);
        }

        foreach($obrisanihOblastiIstrazivanja as $oblast_istrazivanja){
            $this->fillResearchAreaInfo($oblast_istrazivanja);
        }

        return view('admin.adminOblastiIstrazivanja', compact('aktivnihOblastiIstrazivanja', 'obrisanihOblastiIstrazivanja'));
    }

    public function saveResearchArea($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];

        $toFill = true;

        if($izmena){
            $oblast_istrazivanja = OblastIstrazivanja::dohvatiSaId($id);

            if($oblast_istrazivanja->naziv == $naziv && $oblast_istrazivanja->opis == $opis ){
                $toFill = false;
            }

        } else{
            $oblast_istrazivanja = new OblastIstrazivanja();
        }

        if($toFill) {
            $oblast_istrazivanja->napuni($naziv, $opis);
        }

        return redirect('/admin/oblast-istrazivanja/' . $oblast_istrazivanja->id);
    }

    public function deleteResearchArea($id){
        $oblast_istrazivanja = OblastIstrazivanja::dohvatiSaId($id);

        $oblast_istrazivanja->obrisi();

        return Redirect::back();
    }

    public function restaurateResearchArea($id){
        $oblast_istrazivanja = OblastIstrazivanja::dohvatiSaId($id);

        $oblast_istrazivanja->restauriraj();

        return Redirect::back();
    }
}
