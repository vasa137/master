<?php

namespace App\Http\Controllers;

use App\Obrazovanje;
use App\ObrazovnaUstanova;
use App\OsobaObrazovanje;
use App\OsobaProjekat;
use App\Projekat;
use Illuminate\Http\Request;
use Redirect;

class AdminEducationInstitutionController extends Controller
{
    private function fillEducationInstitutionInfo($obrazovna_ustanova){
        $projekti = Projekat::dohvatiSvePoslate();

        $broj_osoba = 0;

        foreach($projekti as $projekat){
            // RUKOVODILAC
            $osobaObrazovanja = OsobaObrazovanje::dohvatiZaOsobu($projekat->id_osoba);

            foreach($osobaObrazovanja as $osobaObrazovanje){
                $obrazovanje = Obrazovanje::dohvatiSaId($osobaObrazovanje->id_obrazovanje);

                if($obrazovanje->id_obrazovna_ustanova == $obrazovna_ustanova->id){
                    $broj_osoba++;
                    break;
                }
            }

            // UCESNICI
            $osobeProjekat = OsobaProjekat::dohvatiZaProjekat($projekat->id);

            foreach($osobeProjekat as $osobaProjekat){
                $osobaObrazovanja = OsobaObrazovanje::dohvatiZaOsobu($osobaProjekat->id_osoba);

                foreach($osobaObrazovanja as $osobaObrazovanje){
                    $obrazovanje = Obrazovanje::dohvatiSaId($osobaObrazovanje->id_obrazovanje);

                    if($obrazovanje->id_obrazovna_ustanova == $obrazovna_ustanova->id){
                        $broj_osoba++;
                        break;
                    }
                }
            }
        }

        $obrazovna_ustanova->broj_osoba = $broj_osoba;
    }

    public function educationInstitution($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        if(!$izmena){
            return view('admin.adminObrazovnaUstanova', compact('izmena'));
        } else{
            $obrazovna_ustanova = ObrazovnaUstanova::dohvatiSaId($id);

            if($obrazovna_ustanova == null){
                abort(404);
            }

            $this->fillEducationInstitutionInfo($obrazovna_ustanova);

            return view('admin.adminObrazovnaUstanova', compact('izmena', 'obrazovna_ustanova'));
        }
    }

    public function educationInstitutions(){
        $aktivnihObrazovnihUstanova = ObrazovnaUstanova::dohvatiSveAktivne();
        $obrisanihObrazovnihUstanova = ObrazovnaUstanova::dohvatiSveObrisane();

        foreach($aktivnihObrazovnihUstanova as $obrazovna_ustanova){
            $this->fillEducationInstitutionInfo($obrazovna_ustanova);
        }

        foreach($obrisanihObrazovnihUstanova as $obrazovna_ustanova){
            $this->fillEducationInstitutionInfo($obrazovna_ustanova);
        }

        return view('admin.adminObrazovneUstanove', compact('aktivnihObrazovnihUstanova', 'obrisanihObrazovnihUstanova'));
    }

    public function saveEducationInstitution($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];

        $toFill = true;

        if($izmena){
            $obrazovna_ustanova = ObrazovnaUstanova::dohvatiSaId($id);

            if($obrazovna_ustanova->naziv == $naziv && $obrazovna_ustanova->opis == $opis ){
                $toFill = false;
            }

        } else{
            $obrazovna_ustanova = new ObrazovnaUstanova();
        }

        if($toFill) {
            $obrazovna_ustanova->napuni($naziv, $opis);
        }

        return redirect('/admin/obrazovna-ustanova/' . $obrazovna_ustanova->id);
    }

    public function deleteEducationInstitution($id){
        $obrazovna_ustanova = ObrazovnaUstanova::dohvatiSaId($id);

        $obrazovna_ustanova->obrisi();

        return Redirect::back();
    }

    public function restaurateEducationInstitution($id){
        $obrazovna_ustanova = ObrazovnaUstanova::dohvatiSaId($id);

        $obrazovna_ustanova->restauriraj();

        return Redirect::back();
    }
}
