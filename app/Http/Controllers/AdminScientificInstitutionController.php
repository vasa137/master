<?php

namespace App\Http\Controllers;

use App\NaucnaUstanova;
use App\OsobaProjekat;
use App\OsobaZaposlenje;
use App\Projekat;
use App\Zaposlenje;
use Illuminate\Http\Request;
use Redirect;
class AdminScientificInstitutionController extends Controller
{
    private function fillScientificInstitutionInfo($naucna_ustanova){
        $projekti = Projekat::dohvatiSvePoslate();

        $broj_osoba = 0;

        foreach($projekti as $projekat){
            // RUKOVODILAC
            $osobaZaposlenja = OsobaZaposlenje::dohvatiZaOsobu($projekat->id_osoba);

            foreach($osobaZaposlenja as $osobaZaposlenje){
                $zaposlenje = Zaposlenje::dohvatiSaId($osobaZaposlenje->id_zaposlenje);

                if($zaposlenje->id_naucna_ustanova == $naucna_ustanova->id){
                    $broj_osoba++;
                    break;
                }
            }

            // UCESNICI
            $osobeProjekat = OsobaProjekat::dohvatiZaProjekat($projekat->id);

            foreach($osobeProjekat as $osobaProjekat){
                $osobaZaposlenja = OsobaZaposlenje::dohvatiZaOsobu($osobaProjekat->id_osoba);

                foreach($osobaZaposlenja as $osobaZaposlenje){
                    $zaposlenje = Zaposlenje::dohvatiSaId($osobaZaposlenje->id_zaposlenje);

                    if($zaposlenje->id_naucna_ustanova == $naucna_ustanova->id){
                        $broj_osoba++;
                        break;
                    }
                }
            }
        }

        $naucna_ustanova->broj_osoba = $broj_osoba;
    }

    public function scientificInstitution($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        if(!$izmena){
            return view('admin.adminNaucnaUstanova', compact('izmena'));
        } else{
            $naucna_ustanova = NaucnaUstanova::dohvatiSaId($id);

            if($naucna_ustanova == null){
                abort(404);
            }

            $this->fillScientificInstitutionInfo($naucna_ustanova);

            return view('admin.adminNaucnaUstanova', compact('izmena', 'naucna_ustanova'));
        }
    }

    public function scientificInstitutions(){
        $aktivnihNaucnihUstanova = NaucnaUstanova::dohvatiSveAktivne();
        $obrisanihNaucnihUstanova = NaucnaUstanova::dohvatiSveObrisane();

        foreach($aktivnihNaucnihUstanova as $naucna_ustanova){
            $this->fillScientificInstitutionInfo($naucna_ustanova);
        }

        foreach($obrisanihNaucnihUstanova as $naucna_ustanova){
            $this->fillScientificInstitutionInfo($naucna_ustanova);
        }

        return view('admin.adminNaucneUstanove', compact('aktivnihNaucnihUstanova', 'obrisanihNaucnihUstanova'));
    }

    public function saveScientificInstitution($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];

        $toFill = true;

        if($izmena){
            $naucna_ustanova = NaucnaUstanova::dohvatiSaId($id);

            if($naucna_ustanova->naziv == $naziv && $naucna_ustanova->opis == $opis ){
                $toFill = false;
            }

        } else{
            $naucna_ustanova = new NaucnaUstanova();
        }

        if($toFill) {
            $naucna_ustanova->napuni($naziv, $opis);
        }

        return redirect('/admin/naucna-ustanova/' . $naucna_ustanova->id);
    }

    public function deleteScientificInstitution($id){
        $naucna_ustanova = NaucnaUstanova::dohvatiSaId($id);

        $naucna_ustanova->obrisi();

        return Redirect::back();
    }

    public function restaurateScientificInstitution($id){
        $naucna_ustanova = NaucnaUstanova::dohvatiSaId($id);

        $naucna_ustanova->restauriraj();

        return Redirect::back();
    }
}
