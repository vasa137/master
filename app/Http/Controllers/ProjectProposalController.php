<?php

namespace App\Http\Controllers;

use App\KategorijaRada;
use App\NaucnaUstanova;
use App\OblastIstrazivanja;
use App\OblastProjekat;
use App\Obrazovanje;
use App\ObrazovnaUstanova;
use App\Osoba;
use App\OsobaOblastIstrazivanja;
use App\OsobaObrazovanje;
use App\OsobaProjekat;
use App\OsobaReferenca;
use App\OsobaZaposlenje;
use App\Projekat;
use App\Referenca;
use App\StepenStudija;
use App\User;
use App\Utility\AdminPodaci;
use App\Utility\FilesDescriptor;
use App\Zaposlenje;
use Illuminate\Http\Request;
use Auth;
use File;
use Validator;
use Zip;
class ProjectProposalController extends Controller
{
    private function clearTempDir($id_user){
        $temp_dir = storage_path('app/public/datoteke/temp/' . $id_user);

        File::deleteDirectory($temp_dir, true);
    }

    private function getDirFiles($dir){
        $files = [];

        foreach (scandir($dir) as $file) {
            if ('.' === $file) continue;
            if ('..' === $file) continue;

            $files[] = $file;
        }

        return $files;
    }

    private function loadEducations($id_osoba, $adminPodaci){
        $obrazovanja = [];

        $osobaObrazovanja = OsobaObrazovanje::dohvatiZaOsobu($id_osoba);

        foreach($osobaObrazovanja as $osobaObrazovanje) {
            $obrazovanje = Obrazovanje::dohvatiSaId($osobaObrazovanje->id_obrazovanje);



            // ZBOG SAKRIVENOSTI I POTENCIJALNOG ODABIRA U SELECTU RANIJE
            $obrazovnaUstanova = ObrazovnaUstanova::dohvatiSaId($obrazovanje->id_obrazovna_ustanova);

            if($obrazovnaUstanova->sakriven){
                $adminPodaci->obrazovneUstanove [] = $obrazovnaUstanova;
            }

            // ZBOG SAKRIVENOSTI I POTENCIJALNOG ODABIRA U SELECTU RANIJE
            $stepenStudija = StepenStudija::dohvatiSaId($obrazovanje->id_stepen_studija);

            if($stepenStudija->sakriven){
                $adminPodaci->stepeniStudija [] = $stepenStudija;
            }

            $obrazovanje->obrazovnaUstanova = $obrazovnaUstanova;
            $obrazovanje->stepenStudija = $stepenStudija;

            $obrazovanja [] = $obrazovanje;
        }

        return $obrazovanja;
    }

    private function loadJobs($id_osoba, $adminPodaci){
        $zaposlenja = [];

        $osobaZaposlenja = OsobaZaposlenje::dohvatiZaOsobu($id_osoba);

        foreach($osobaZaposlenja as $osobaZaposlenje) {
            $zaposlenje = Zaposlenje::dohvatiSaId($osobaZaposlenje->id_zaposlenje);


            // ZBOG SAKRIVENOSTI I POTENCIJALNOG ODABIRA U SELECTU RANIJE
            $naucnaUstanova = NaucnaUstanova::dohvatiSaId($zaposlenje->id_naucna_ustanova);

            if($naucnaUstanova->sakriven){
                $adminPodaci->naucneUstanove [] = $naucnaUstanova;
            }


            $zaposlenje->naucnaUstanova = $naucnaUstanova;

            $zaposlenja [] = $zaposlenje;
        }

        return $zaposlenja;
    }

    private function loadReferences($id_osoba, $adminPodaci){
        $reference = [];

        $osobaReference = OsobaReferenca::dohvatiZaOsobu($id_osoba);

        foreach($osobaReference as $osobaReferenca) {
            $referenca = Referenca::dohvatiSaId($osobaReferenca->id_referenca);


            // ZBOG SAKRIVENOSTI I POTENCIJALNOG ODABIRA U SELECTU RANIJE
            $kategorijaRada = KategorijaRada::dohvatiSaId($referenca->id_kategorija_rada);

            if($kategorijaRada->sakriven){
                $adminPodaci->kategorijeRada [] = $kategorijaRada;
            }



            $referenca->kategorijaRada = $kategorijaRada;

            $reference [] = $referenca;
        }

        return $reference;
    }

    private function loadMember($id_osoba, $adminPodaci){
        $osoba = Osoba::dohvatiSaId($id_osoba);

        $oblastiIstrazivanja = [];

        $osobaOblastIstrazivanja = OsobaOblastIstrazivanja::dohvatiZaOsobu($id_osoba);

        foreach($osobaOblastIstrazivanja as $osobaOblastIstrazivanje) {
            $oblastIstrazivanja = OblastIstrazivanja::dohvatiSaId($osobaOblastIstrazivanje->id_oblast_istrazivanja);

            // AKO SE DESILO DA JE SELECTOVAN A POSTAO SAKRIVEN
            if($oblastIstrazivanja->sakriven){
                $adminPodaci->oblastiIstrazivanja[] =  $oblastIstrazivanja;
            }

            $oblastiIstrazivanja [] = $oblastIstrazivanja;
        }

        $osoba->oblastiIstrazivanja = $oblastiIstrazivanja;
        $osoba->obrazovanja = $this->loadEducations($id_osoba, $adminPodaci);
        $osoba->zaposlenja = $this->loadJobs($id_osoba, $adminPodaci);
        $osoba->reference = $this->loadReferences($id_osoba, $adminPodaci);

        return $osoba;
    }

    private function loadMembers($projekat, $adminPodaci){
        $osobe = [];

        $osobe [] = $this->loadMember($projekat->id_osoba, $adminPodaci);

        $osobeProjekat = OsobaProjekat::dohvatiZaProjekat($projekat->id);

        foreach($osobeProjekat as $osobaProjekat){
            $id_osoba = $osobaProjekat->id_osoba;

            $osobe [] = $this->loadMember($id_osoba, $adminPodaci);
        }

        $projekat->osobe = $osobe;
    }

    private function loadFiles($projekat){
        $projectFiles = [];
        $bibliography = [];
        $biography = [];

        for($i = 0; $i < Projekat::$PROJECT_FILES_NUM; $i++){
            $projectFiles[$i] = [];
        }

        for($i = 0; $i < Projekat::$MAX_MEMBERS; $i++){
            $bibliography[$i] = [];
            $biography[$i] = [];
        }

        $project_dir = storage_path('app/public/datoteke/' . $projekat->id);

        $temp_dir = storage_path('app/public/datoteke/temp/' . $projekat->id_user);

        $nestedProjectDirs = $this->getDirFiles($project_dir);

        foreach($nestedProjectDirs as $dir){
            $index = $this->getFileDescriptorDirIndex($dir);

            // ne sme index != null, jer je 0 kod int-a isto sto i null
            if($index != -1){
                $projectFiles[$index] = $this->getDirFiles($project_dir . '/' . $dir);

                File::makeDirectory($temp_dir . '/' . $dir, 0755, true);

                File::copyDirectory($project_dir . '/' . $dir, $temp_dir . '/' . $dir);
            } else{
                if($dir == 'ucesnici'){
                    $membersDirs = $this->getDirFiles($project_dir . '/' . $dir);

                    foreach($membersDirs as $memberDir){
                        $nestedMemberDirs = $this->getDirFiles($project_dir . '/' . $dir . '/' . $memberDir);

                        foreach($nestedMemberDirs as $nestedMemberDir){
                            if(!File::exists($temp_dir . '/' . $dir)){
                                File::makeDirectory($temp_dir . '/' . $dir . '/' . $memberDir . '/' . $nestedMemberDir, 0755, true);
                            }

                            File::copyDirectory($project_dir . '/' . $dir . '/' . $memberDir . '/' . $nestedMemberDir, $temp_dir . '/' . $dir . '/' . $memberDir . '/' . $nestedMemberDir);

                            if($nestedMemberDir == 'bibliografija'){
                                $bibliography[$memberDir] = $this->getDirFiles($project_dir . '/' . $dir . '/' . $memberDir . '/' . $nestedMemberDir);
                            } else if($nestedMemberDir == 'biografija'){
                                $biography[$memberDir] = $this->getDirFiles($project_dir . '/' . $dir . '/' . $memberDir . '/' . $nestedMemberDir);
                            }
                        }
                    }
                }
            }
        }

        $filesDescriptor = new FilesDescriptor($projectFiles, $bibliography, $biography);

        $projekat->filesDescriptor = $filesDescriptor;
    }

    public function projectApplication($id){
        $edit = false;

        if($id > 0){
            $edit = true;
        }

        $id_user =  Auth::user()->id;

        $this->clearTempDir($id_user);

        $oblastiProjekta = OblastProjekat::dohvatiSveAktivne();

        $oblastiIstrazivanja = OblastIstrazivanja::dohvatiSveAktivne();

        $stepeniStudija = StepenStudija::dohvatiSveAktivne();

        $naucneUstanove = NaucnaUstanova::dohvatiSveAktivne();

        $obrazovneUstanove = ObrazovnaUstanova::dohvatiSveAktivne();

        $kategorijeRada = KategorijaRada::dohvatiSveAktivne();

        $adminPodaci = new AdminPodaci($oblastiProjekta, $oblastiIstrazivanja, $stepeniStudija, $naucneUstanove, $obrazovneUstanove, $kategorijeRada);

        if(!$edit){
            return view('prijava', compact('edit','id', 'adminPodaci'));
        } else{
            $projekat = Projekat::dohvatiSaId($id);

            if($projekat == null || $projekat->id_user != $id_user){
                return abort(404);
            }

            $projekat->user = User::dohvatiSaId($projekat->id_user);

            // ZBOG SELECTA I SAKRIVENOSTI
            $oblastProjekta = OblastProjekat::dohvatiSaId($projekat->id_oblast_projekat);

            if($oblastProjekta != null && $oblastProjekta->sakriven){
                $adminPodaci->oblastiProjekta [] = $oblastProjekta;
            }

            // SALJU SE ADMIN PODACI UKOLIKO JE NEKI OD PODATAKA POSTAO SAKRIVEN DA BI SE PRIKAZAO UKOLIKO JE ODABRAN U SELECTOVIMA
            $this->loadMembers($projekat, $adminPodaci);

            $this->loadFiles($projekat);

            return view('prijava', compact('projekat', 'edit','id', 'adminPodaci'));
        }
    }

    public function uploadProjectFile(Request $request, $fileDirToSave){
        // EXCEL
        if($fileDirToSave == 'budzet'){
            $validator = Validator::make($request->all(), [
                "file"    => "required|file|max:10485760|mimetypes:text/plain,text/csv,application/application/vnd.ms-office,vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            ]);
        } else{ //PDF
            $validator = Validator::make($request->all(), [
                "file"    => "required|file|max:10485760|mimes:pdf",
            ]);
        }

        if(!$validator->passes()){
            abort(500);
        }

        $path_info = pathinfo($_FILES['file']['name']);

        $extension = strtolower($path_info['extension']);

        // Mora se proveriti i ekstenzija zbog mimeTipova za EXCEL
        if($fileDirToSave == 'budzet'){
            if(!($extension == 'csv' || $extension == 'xls' || $extension == 'xlsx')){
                abort(500);
            }
        } else{
            if($extension != 'pdf'){
                abort(500);
            }
        }

        $file = $_FILES['file'];

        $datoteke_temp_dir = storage_path('app/public/datoteke/temp');

        $id_user = Auth::user()->id;

        $dir_to_save = $datoteke_temp_dir . '/' . $id_user . '/' . $fileDirToSave;

        $file_full_path = $dir_to_save . '/' . $file['name'];

        if(!File::exists($dir_to_save)){
            File::makeDirectory($dir_to_save,  0755, true);
        }

        // brise sadrzaj direktorijuma, a direktorijum ostaje
        File::deleteDirectory($dir_to_save, true);

        File::move($file['tmp_name'], $file_full_path);

        chmod($file_full_path, 0644);
    }

    public function uploadStatementFiles(Request $request){
        $validator = Validator::make($request->all(), [
            "files.*"    => "required|file|max:5242880|mimes:pdf",
        ]);

        if(!$validator->passes()){
            abort(500);
        }

        $files = $_FILES['files'];

        $count = count($files['name']);

        $datoteke_temp_dir = storage_path('app/public/datoteke/temp');

        $id_user = Auth::user()->id;

        $dir_to_save = $datoteke_temp_dir . '/' . $id_user . '/izjave';

        if(!File::exists($dir_to_save)){
            File::makeDirectory($dir_to_save,  0755, true);
        }

        // brise sadrzaj direktorijuma, a direktorijum ostaje
        File::deleteDirectory($dir_to_save, true);

        for($i = 0; $i < $count; $i++) {
            $file_full_path = $dir_to_save . '/' . $files['name'][$i];

            File::move($files['tmp_name'][$i], $file_full_path);

            chmod($file_full_path, 0644);
        }
    }

    public function uploadPersonFile(Request $request, $personId, $fileDirToSave){
        $validator = Validator::make($request->all(), [
            "file"    => "required|file|max:5242880|mimes:pdf",
        ]);

        if(!$validator->passes()){
            abort(500);
        }

        $file = $_FILES['file'];

        $datoteke_temp_dir = storage_path('app/public/datoteke/temp');

        $id_user = Auth::user()->id;

        $dir_to_save = $datoteke_temp_dir . '/' . $id_user . '/ucesnici/' . $personId . '/' . $fileDirToSave;

        $file_full_path = $dir_to_save . '/' . $file['name'];

        if(!File::exists($dir_to_save)){
            File::makeDirectory($dir_to_save,  0755, true);
        }

        // brise sadrzaj direktorijuma, a direktorijum ostaje
        File::deleteDirectory($dir_to_save, true);

        File::move($file['tmp_name'], $file_full_path);

        chmod($file_full_path, 0644);
    }

    private function validateDraftFields($project, $isFinal){

        $validator = Validator::make($project, [
            "title"    => "required|string|max:255",
            "abstract"  => "string|max:2000" . ($isFinal ? '|required' : ''),
            "id_area_project" => "numeric|" . ($isFinal ? 'required' : 'nullable'),
            "budget" => "numeric|max:200000|" . ($isFinal ? 'required' : 'nullable'),
            "members" => "array|max:6|required",
            "members.*.firstName" => "string|max:63" . ($isFinal ? '|required' : ''),
            "members.*.lastName" => "string|max:63" . ($isFinal ? '|required' : ''),
            "members.*.birthDate" => "date|date_format:Y-m-d|before:today|" . ($isFinal ? 'required' : 'nullable'),
            "members.*.id_area_research" => "array|max:5|" . ($isFinal ? 'required' : 'nullable'),
            "members.*.id_area_research.*" => "numeric",
            "members.*.educations" => "array" . ($isFinal ? '|required' : ''),
            "members.*.educations.*.id_education_institution" => "required|numeric",
            "members.*.educations.*.id_studies_degree" => "required|numeric",
            "members.*.educations.*.title" => "required|string|max:255",
            "members.*.educations.*.date" => "required|date|date_format:Y-m-d|before:today",
            "members.*.jobs" => "array",
            "members.*.jobs.*.date_start" => "required|date|date_format:Y-m-d|before:today",
            "members.*.jobs.*.date_end" => "date|nullable|date_format:Y-m-d|before:today",
            "members.*.jobs.*.id_scientific_institution" => "required|numeric",
            "members.*.jobs.*.position" => "required|string|max:255",
            "members.*.references" => "array|max:5",
            "members.*.references.*.identifier" => "required|string|max:255",
            "members.*.references.*.title" => "required|string|max:255",
            "members.*.references.*.id_category_reference" => "required|numeric",
            "members.*.references.*.announcement_places" => "required|string|max:1000",
            "members.*.references.*.signers" => "required|string|max:255",
            "filesDescriptor" => "required|array|size:4",
            "filesDescriptor.projectFilesFlags" => "required|array|size:5",
            "filesDescriptor.projectFilesFlags.*" => "boolean" . ($isFinal ? '|in:1' : ''),
            "filesDescriptor.statementFilesCount" => "numeric|max:6|" . ($isFinal ? 'min:1' : 'min:0'),
            "filesDescriptor.bibliographyFlags" => "required|array|size:6",
            "filesDescriptor.bibliographyFlags.*" => "boolean",
            "filesDescriptor.biographyFlags" => "required|array|size:6",
            "filesDescriptor.biographyFlags.*" => "boolean",
            "conditionsAgreed" => "boolean|required" .  ($isFinal ? '|in:1' : ''),
        ]);

        if($validator->passes() && $isFinal){
            for($i = 0; $i < count($project['members']); $i++){
                if(isset($project['members'][$i])){
                    // ZA SVE CLANOVE KOJI POSTOJE MORA SE ISPITATI DA LI SU IM POSTAVLJENI BIBLIOGRAFIJA I BIOGRAFIJA
                    if(!($project['filesDescriptor']['bibliographyFlags'][$i] && $project['filesDescriptor']['biographyFlags'][$i])){
                        return false;
                    }
                }
            }
        }

        return $validator->passes();
    }

    private function validateDraftProject($project, $edit, $id){
        if($edit){
            $id_user = Auth::user()->id;

            $projekat = Projekat::dohvatiSaId($id);

            if($projekat == null || $projekat->id_user != $id_user || $projekat->poslata == true){
                return false;
            }
        }

        if($project['id_area_project'] != null) {
            $areaProject = OblastProjekat::dohvatiSaId($project['id_area_project']);

            if($areaProject == null) {
                return false;
            }
        }

        return true;
    }

    private function validateDraftMember($member){

        if($member['birthDate'] != null) {
            $dateObject = new \DateTime($member['birthDate'], new \DateTimeZone('Europe/Belgrade'));
            $today = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
            $diff = $today->diff($dateObject);

            // minimum 18 godina mora imati ucesnik
            if ($diff->y < 18) {
                return false;
            }
        }

        if($member['id_area_research'] != null) {

            $arrayAreaResearch = $member['id_area_research'];

            foreach ($arrayAreaResearch as $idAreaResearch) {
                $areaResearch = OblastIstrazivanja::dohvatiSaId($idAreaResearch);

                if ($areaResearch == null) {
                    return false;
                }
            }
        }

        return true;
    }

    private function validateDraftMemberEducations($member){
        for($i = 0; $i < count($member['educations']); $i++){
            $education = $member['educations'][$i];

            $educationInstitution = ObrazovnaUstanova::dohvatiSaId($education['id_education_institution']);

            if($educationInstitution == null){
                return false;
            }

            $studiesDegree = StepenStudija::dohvatiSaId($education['id_studies_degree']);

            if($studiesDegree == null){
                return false;
            }
        }

        return true;
    }

    private function validateDraftMemberJobs($member){
        for($i = 0; $i < count($member['jobs']); $i++){
            $job = $member['jobs'][$i];

            if($job['date_end'] != null){
                $dateEndObject = new \DateTime($job['date_end'], new \DateTimeZone('Europe/Belgrade'));
                $dateStartObject = new \DateTime($job['date_start'], new \DateTimeZone('Europe/Belgrade'));

                $diff = $dateEndObject->diff($dateStartObject);

                if($diff->d < 0){
                    return false;
                }
            }

            $scientificInstitution = NaucnaUstanova::dohvatiSaId($job['id_scientific_institution']);

            if($scientificInstitution == null){
                return false;
            }
        }

        return true;
    }

    private function validateDraftMemberReferences($member){
        for($i = 0; $i < count($member['references']); $i++){
            $reference = $member['references'][$i];

            $workCategory = KategorijaRada::dohvatiSaId($reference['id_category_reference']);

            if($workCategory == null){
                return false;
            }
        }

        return true;
    }

    private function validateDraftFiles($filesDescriptor){
        $id_user =  Auth::user()->id;

        $temp_dir = storage_path('app/public/datoteke/temp/' . $id_user);

        for($i = 0; $i < count($filesDescriptor['projectFilesFlags']); $i++){
            if($filesDescriptor['projectFilesFlags'][$i]){
                $file_dir_name = $this->getFileDescriptorDirName($i);

                $file_temp_dir = $temp_dir . '/' . $file_dir_name;

                if($file_dir_name == 'izjave'){
                    $fileNumToCheck = $filesDescriptor['statementFilesCount'];
                }
                else{
                    $fileNumToCheck = 1;
                }

                if(count($this->getDirFiles($file_temp_dir)) != $fileNumToCheck){
                    return false;
                }
            }
        }

        for($i = 0; $i < count($filesDescriptor['bibliographyFlags']); $i++) {
            if($filesDescriptor['bibliographyFlags'][$i]){
                $file_dir_name = 'ucesnici/' .  $i . '/bibliografija';

                $file_temp_dir = $temp_dir . '/' . $file_dir_name;

                $fileNumToCheck = 1;

                if(count($this->getDirFiles($file_temp_dir)) != $fileNumToCheck){
                    return false;
                }
            }
        }

        for($i = 0; $i < count($filesDescriptor['biographyFlags']); $i++) {
            if($filesDescriptor['biographyFlags'][$i]){
                $file_dir_name = 'ucesnici/' .  $i . '/biografija';

                $file_temp_dir = $temp_dir . '/' . $file_dir_name;

                $fileNumToCheck = 1;

                if(count($this->getDirFiles($file_temp_dir)) != $fileNumToCheck){
                    return false;
                }
            }
        }

        return true;
    }

    private function validateDraft($project, $edit, $id, $isFinal){

        if(!$this->validateDraftFields($project, $isFinal)){
            return false;
        }

        if(!$this->validateDraftProject($project, $edit, $id)){
            return false;
        }

        for($i = 0; $i < count($project['members']); $i++){
            if(isset($project['members'][$i])){
                $member = $project['members'][$i];

                if(!$this->validateDraftMember($member)){
                    return false;
                }

                if(!$this->validateDraftMemberEducations($member)){
                    return false;
                }

                if(!$this->validateDraftMemberJobs($member)){
                    return false;
                }

                if(!$this->validateDraftMemberReferences($member)){
                    return false;
                }

            }
        }

        if(!$this->validateDraftFiles($project['filesDescriptor'])){
            return false;
        }

        return true;
    }

    private function fillEducation($osoba, $education){
        $obrazovanje = new Obrazovanje();

        $obrazovanje->napuni($education['id_education_institution'], $education['id_studies_degree'], $education['title'], $education['date']);

        $osobaObrazovanje = new OsobaObrazovanje();

        $osobaObrazovanje->napuni($osoba->id, $obrazovanje->id);
    }

    private function fillJob($osoba, $job){
        $zaposlenje = new Zaposlenje();

        $zaposlenje->napuni($job['date_start'], $job['date_end'], $job['id_scientific_institution'], $job['position']);

        $osobaZaposlenje = new OsobaZaposlenje();

        $osobaZaposlenje->napuni($osoba->id, $zaposlenje->id);
    }

    private function fillReference($osoba, $reference){
        $referenca = new Referenca();

        $referenca->napuni($reference['identifier'], $reference['title'], $reference['id_category_reference'], $reference['announcement_places'], $reference['signers']);

        $osobaReferenca = new OsobaReferenca();

        $osobaReferenca->napuni($osoba->id, $referenca->id);
    }

    private function fillMember($member){
        $osoba = new Osoba();

        $osoba->napuni($member['firstName'], $member['lastName'], $member['birthDate']);

        $arrayResearchArea = $member['id_area_research'];

        if($arrayResearchArea != null) {
            foreach ($arrayResearchArea as $id_research_area) {
                $osobaOblastIstrazivanja = new OsobaOblastIstrazivanja();

                $osobaOblastIstrazivanja->napuni($osoba->id, $id_research_area);
            }
        }

        foreach($member['educations'] as $education){
            $this->fillEducation($osoba, $education);
        }

        foreach($member['jobs'] as $job){
            $this->fillJob($osoba, $job);
        }

        foreach($member['references'] as $reference){
            $this->fillReference($osoba, $reference);
        }

        return $osoba;
    }

    private function fillProject($project, $osobe, $edit, $id,  $isFinal){
        if($edit){
            $projekat = Projekat::dohvatiSaId($id);
        } else{
            $projekat = new Projekat();
        }

        $id_user = Auth::user()->id;

        $projekat->napuni($project['title'], $project['abstract'], $project['id_area_project'], $project['budget'], $osobe[0]->id, $id_user, $isFinal);

        for($i = 1; $i < count($osobe); $i++){
            $osoba = $osobe[$i];

            $osobaProjekat = new OsobaProjekat();

            $osobaProjekat->napuni($osoba->id, $projekat->id);
        }

        return $projekat;
    }

    private function getFileDescriptorDirIndex($file_dir_name){
        $index = -1;
        switch ($file_dir_name){
            case 'opis':
                $index = 0;
                break;
            case 'budzet':
                $index = 1;
                break;
            case 'gantogram':
                $index = 2;
                break;
            case 'prezentacija':
                $index = 3;
                break;
            case 'izjave':
                $index = 4;
                break;
        }

        return $index;
    }

    private function getFileDescriptorDirName($i){
        $file_dir_name = '';
        switch ($i){
            case 0:
                $file_dir_name = 'opis';
                break;
            case 1:
                $file_dir_name = 'budzet';
                break;
            case 2:
                $file_dir_name = 'gantogram';
                break;
            case 3:
                $file_dir_name = 'prezentacija';
                break;
            case 4:
                $file_dir_name = 'izjave';
                break;
        }

        return $file_dir_name;
    }

    private function copyTempFiles($project_dir, $filesDescriptor, $id_user){
        $temp_dir = storage_path('app/public/datoteke/temp/' . $id_user);

        for($i = 0; $i < count($filesDescriptor['projectFilesFlags']); $i++){
            if($filesDescriptor['projectFilesFlags'][$i]){
                $file_dir_name = $this->getFileDescriptorDirName($i);

                $file_temp_dir = $temp_dir . '/' . $file_dir_name;

                $file_dir_destination = $project_dir . '/' . $file_dir_name;;

                if(!File::exists($file_dir_destination)) {
                    File::makeDirectory($file_dir_destination, 0755, true);
                }

                // copy directory prebacuje sadrzaj direktorijuma u direktorijum koji vec postoji
                File::copyDirectory($file_temp_dir, $file_dir_destination);
            }
        }

        $personId = 0;

        for($i = 0; $i < count($filesDescriptor['bibliographyFlags']); $i++) {
            if($filesDescriptor['bibliographyFlags'][$i]){
                // $i je peopleId sa frontend-a, tu ima preskoka, a personId je novi id koji se dodeljuje redom
                $file_dir_name = 'ucesnici/' .  $personId . '/bibliografija';

                $file_temp_dir = $temp_dir . '/ucesnici/' .  $i . '/bibliografija';

                $file_dir_destination = $project_dir . '/' . $file_dir_name;

                if(!File::exists($file_dir_destination)){
                    File::makeDirectory($file_dir_destination, 0755, true);
                }

                // copy directory prebacuje sadrzaj direktorijuma u direktorijum koji vec postoji
                File::copyDirectory($file_temp_dir, $file_dir_destination);

                $personId++;
            }
        }

        $personId = 0;

        for($i = 0; $i < count($filesDescriptor['biographyFlags']); $i++) {
            if($filesDescriptor['biographyFlags'][$i]){
                $file_dir_name = 'ucesnici/' .  $personId . '/biografija';

                $file_temp_dir = $temp_dir . '/ucesnici/' .  $i . '/biografija';

                $file_dir_destination = $project_dir . '/' . $file_dir_name;

                if(!File::exists($file_dir_destination)){
                    File::makeDirectory($file_dir_destination, 0755, true);
                }

                // copy directory prebacuje sadrzaj direktorijuma u direktorijum koji vec postoji
                File::copyDirectory($file_temp_dir, $file_dir_destination);

                $personId++;
            }
        }
    }

    private function deleteEducations($id_osoba){
        $osobaObrazovanja = OsobaObrazovanje::dohvatiZaOsobu($id_osoba);

        foreach($osobaObrazovanja as $osobaObrazovanje) {
            $obrazovanje = Obrazovanje::dohvatiSaId($osobaObrazovanje->id_obrazovanje);

            $osobaObrazovanje->obrisi();

            $obrazovanje->obrisi();
        }

    }

    private function deleteJobs($id_osoba){
        $osobaZaposlenja = OsobaZaposlenje::dohvatiZaOsobu($id_osoba);

        foreach($osobaZaposlenja as $osobaZaposlenje) {
            $zaposlenje = Zaposlenje::dohvatiSaId($osobaZaposlenje->id_zaposlenje);

            $osobaZaposlenje->obrisi();

            $zaposlenje->obrisi();
        }
    }

    private function deleteReferences($id_osoba){
        $osobaReference = OsobaReferenca::dohvatiZaOsobu($id_osoba);

        foreach($osobaReference as $osobaReferenca) {
            $referenca = Referenca::dohvatiSaId($osobaReferenca->id_referenca);

            $osobaReferenca->obrisi();

            $referenca->obrisi();
        }
    }

    private function deleteMember($id_osoba){
        $osobaOblastIstrazivanja = OsobaOblastIstrazivanja::dohvatiZaOsobu($id_osoba);

        foreach($osobaOblastIstrazivanja as $osobaOblastIstrazivanje) {
            $osobaOblastIstrazivanje->obrisi();
        }

        $this->deleteEducations($id_osoba);
        $this->deleteJobs($id_osoba);
        $this->deleteReferences($id_osoba);

        $osoba = Osoba::dohvatiSaId($id_osoba);

        $osoba->obrisi();
    }

    private function deleteProjectMembers($id, $id_user){
        $projekat = Projekat::dohvatiSaId($id);

        if($projekat == null || $projekat->id_user != $id_user){
            abort(500);
        }

        $id_osoba = $projekat->id_osoba;

        // SAMO PRIVREMENO DA BI MOGAO DA SE IZBRISI RUKOVODILAC
        $projekat->id_osoba = null;

        $projekat->save();

        $this->deleteMember($id_osoba);

        $osobeProjekat = OsobaProjekat::dohvatiZaProjekat($projekat->id);

        foreach($osobeProjekat as $osobaProjekat){
            $id_osoba = $osobaProjekat->id_osoba;

            $osobaProjekat->obrisi();

            $this->deleteMember($id_osoba);
        }
    }

    public function saveProject(Request $request, $id, $isFinal){
        if($isFinal == "1"){
            $isFinal = true;
        } else{
            $isFinal = false;
        }

        $project = json_decode($request->getContent(), true);

        $id_user = Auth::user()->id;

        $edit = false;

        if($id > 0){
            $edit = true;
        }

        if(!$this->validateDraft($project, $edit, $id, $isFinal)){
            abort(500);
        }

        if($edit) {
            $this->deleteProjectMembers($id, $id_user);

            $project_dir = storage_path('app/public/datoteke/' . $id);

            File::deleteDirectory($project_dir, true);
        }

        $osobe = [];

        for($i = 0; $i < 6; $i++) {
            if (isset($project['members'][$i])){
                $osoba = $this->fillMember($project['members'][$i]);

                $osobe[] = $osoba;
            }
        }

        $projekat = $this->fillProject($project, $osobe, $edit, $id, $isFinal);

        $project_dir = storage_path('app/public/datoteke/' . $projekat->id);

        $filesDescriptor = $project['filesDescriptor'];

        $this->copyTempFiles($project_dir, $filesDescriptor, $id_user);

        return $projekat->id;
    }

    public function downloadProjectArtifacts($id){
        $projekat = Projekat::dohvatiSaId($id);

        $id_user = Auth::user()->id;

        $user = User::dohvatiSaId($id_user);

        if($projekat == null || ($projekat->id_user != $id_user && !$user->admin)) {
            abort(404);
        }

        $project_dir = storage_path('app/public/datoteke/' . $projekat->id);

        $artifactsZip = Zip::create($project_dir . "/Projekat " . $id . ' - ' . $projekat->naslov . ".zip");

        try {
            $artifactsZip->add($project_dir, true);
            //BACA NoError Exception ako je sve u redu
        }catch(\Exception $exc){

        }

        $artifactsZip->close();

        return response()->download($project_dir . "/Projekat " . $id . ' - ' . $projekat->naslov . ".zip")->deleteFileAfterSend(true);
    }

    public function myProjects(){
        $id_user = Auth::user()->id;

        $projekti = Projekat::dohvatiProjekteZaUsera($id_user);

        $drafts = [];
        $submitted = [];

        foreach($projekti as $projekat){
            if($projekat->poslata == 1){
                $submitted[] = $projekat;
            } else{
                $drafts[] = $projekat;
            }

            $osobe = [Osoba::dohvatiSaId($projekat->id_osoba)];

            $projekat->osobe = $osobe;
        }

        return view('mojiProjekti', compact('drafts', 'submitted'));
    }
}
