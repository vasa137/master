<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 05-Apr-19
 * Time: 22:10
 */

namespace App\Http\Controllers;
use App\OblastProjekat;
use App\Projekat;
use Redirect;

class AdminProjectAreaController
{

    private function fillProjectAreaInfo($oblast_projekat){
        $oblast_projekat->broj_projekata = Projekat::dohvatiBrojProjekataZaOblastProjekta($oblast_projekat->id);
    }

    public function projectArea($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        if(!$izmena){
            return view('admin.adminOblastProjekat', compact('izmena'));
        } else{
            $oblast_projekat = OblastProjekat::dohvatiSaId($id);

            if($oblast_projekat == null){
                abort(404);
            }

            $this->fillProjectAreaInfo($oblast_projekat);

            return view('admin.adminOblastProjekat', compact('izmena', 'oblast_projekat'));
        }
    }

    public function projectAreas(){
        $aktivnihOblastiProjekata = OblastProjekat::dohvatiSveAktivne();
        $obrisanihOblastiProjekata = OblastProjekat::dohvatiSveObrisane();

        foreach($aktivnihOblastiProjekata as $oblast_projekat){
            $this->fillProjectAreaInfo($oblast_projekat);
        }

        foreach($obrisanihOblastiProjekata as $oblast_projekat){
            $this->fillProjectAreaInfo($oblast_projekat);
        }

        return view('admin.adminOblastiProjekata', compact('aktivnihOblastiProjekata', 'obrisanihOblastiProjekata'));
    }

    public function saveProjectArea($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];

        $toFill = true;

        if($izmena){
            $oblast_projekat = OblastProjekat::dohvatiSaId($id);

            if($oblast_projekat->naziv == $naziv && $oblast_projekat->opis == $opis ){
                $toFill = false;
            }

        } else{
            $oblast_projekat = new OblastProjekat();
        }

        if($toFill) {
            $oblast_projekat->napuni($naziv, $opis);
        }

        return redirect('/admin/oblast-projekat/' . $oblast_projekat->id);
    }

    public function deleteProjectArea($id){
        $oblast_projekat = OblastProjekat::dohvatiSaId($id);

        $oblast_projekat->obrisi();

        return Redirect::back();
    }

    public function restaurateProjectArea($id){
        $oblast_projekat = OblastProjekat::dohvatiSaId($id);

        $oblast_projekat->restauriraj();

        return Redirect::back();
    }
}