<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OblastProjekat extends Model
{
    protected $table = 'oblast_projekat';
    protected $fillable = ['naziv', 'opis', 'sakriven'];

    protected $appends = ['broj_projekata'];

    private $broj_projekata;

    public function getBrojProjekataAttribute()
    {
        return $this->broj_projekata;
    }

    public function setBrojProjekataAttribute($broj_projekata)
    {
        $this->broj_projekata = $broj_projekata;
    }

    public static function dohvatiSaId($id){
        return OblastProjekat::where('id', $id)->first();
    }

    public static function dohvatiMedjuAktivnim($id){
        return OblastProjekat::where('id', $id)->where('sakriven', 0)->first();
    }

    public static function dohvatiSveAktivne(){
        return OblastProjekat::where('sakriven', 0)->orderBy('naziv', 'asc')->get();
    }

    public static function dohvatiSveObrisane(){
        return OblastProjekat::where('sakriven', 1)->orderBy('naziv', 'asc')->get();
    }

    public function napuni($naziv, $opis){
        $this->naziv = $naziv;
        $this->opis = $opis;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }

}
