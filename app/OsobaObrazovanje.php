<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsobaObrazovanje extends Model
{
    protected $table = 'osoba_obrazovanje';
    protected $fillable = ['id_osoba', 'id_obrazovanje'];

    public static function dohvatiZaOsobu($id_osoba){
        return OsobaObrazovanje::where('id_osoba', $id_osoba)->get();
    }

    public function napuni($id_osoba, $id_obrazovanje){
        $this->id_osoba = $id_osoba;
        $this->id_obrazovanje = $id_obrazovanje;

        $this->save();
    }

    public function obrisi(){
        OsobaObrazovanje::where('id_osoba', $this->id_osoba)->where('id_obrazovanje', $this->id_obrazovanje)->delete();
    }
}
