<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategorijaRada extends Model
{
    protected $table = 'kategorija_rada';
    protected $fillable = ['naziv', 'opis', 'sakriven'];

    protected $appends = ['broj_radova'];

    private $broj_radova;

    public function getBrojRadovaAttribute()
    {
        return $this->broj_radova;
    }

    public function setBrojRadovaAttribute($broj_radova)
    {
        $this->broj_radova = $broj_radova;
    }

    public static function dohvatiSaId($id){
        return KategorijaRada::where('id', $id)->first();
    }

    public static function dohvatiMedjuAktivnim($id){
        return KategorijaRada::where('id', $id)->where('sakriven', 0)->first();
    }

    public static function dohvatiSveAktivne(){
        return KategorijaRada::where('sakriven', 0)->orderBy('naziv', 'asc')->get();
    }

    public static function dohvatiSveObrisane(){
        return KategorijaRada::where('sakriven', 1)->orderBy('naziv', 'asc')->get();
    }

    public function napuni($naziv, $opis){
        $this->naziv = $naziv;
        $this->opis = $opis;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }
}
