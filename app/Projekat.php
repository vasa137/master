<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Projekat extends Model
{
    protected $table = 'projekat';
    protected $fillable = ['naslov', 'apstrakt', 'id_oblast_projekat', 'budzet', 'id_osoba', 'id_user', 'poslata'];

    protected $appends = ['osobe', 'filesDescriptor', 'oblast_projekat', 'user'];

    public static $MAX_MEMBERS = 6;
    public static $PROJECT_FILES_NUM = 5;

    private $osobe;
    private $filesDescriptor;
    private $oblast_projekat;
    private $user;

    public function getOsobeAttribute(){
        return $this->osobe;
    }

    public function setOsobeAttribute($osobe){
        $this->osobe = $osobe;
    }

    public function getFilesDescriptorAttribute()
    {
        return $this->filesDescriptor;
    }

    public function setFilesDescriptorAttribute($filesDescriptor)
    {
        $this->filesDescriptor = $filesDescriptor;
    }

    public function getOblastProjekatAttribute()
    {
        return $this->oblast_projekat;
    }

    public function setOblastProjekatAttribute($oblast_projekat)
    {
        $this->oblast_projekat = $oblast_projekat;
    }

    public function getUserAttribute()
    {
        return $this->user;
    }

    public function setUserAttribute($user)
    {
        $this->user = $user;
    }

    public static function dohvatiSaId($id){
        return Projekat::where('id', $id)->first();
    }

    public function napuni($naslov, $apstrakt, $id_oblast_projekat, $budzet, $id_osoba, $id_user, $poslata){
        $this->naslov = $naslov;
        $this->apstrakt = $apstrakt;
        $this->id_oblast_projekat = $id_oblast_projekat;
        $this->budzet = $budzet;
        $this->id_osoba = $id_osoba;
        $this->id_user = $id_user;
        $this->poslata = $poslata;

        $this->save();
    }

    public static function dohvatiProjekteZaUsera($id_user){
        return Projekat::where('id_user', $id_user)->orderBy('updated_at', 'desc')->get();
    }

    public static function dohvatiBrojProjekataZaOblastProjekta($id_oblast_projekat){
        return Projekat::where('poslata', 1)->where('id_oblast_projekat', $id_oblast_projekat)->count();
    }

    public static function dohvatiSvePoslate(){
        return Projekat::where('poslata', 1)->get();
    }

    public static function dohvatiUkupanIznosZaDatum($datum){
        return DB::select("
            select IFNULL(SUM(p.budzet),0) as suma
            from projekat p 
            where p.poslata = 1
            and DATE(p.updated_at) = '$datum'
        ")[0]->suma;
    }

    // whereDate da se ne bi racunao ceo timestamp
    public static function dohvatiBrojPorudzbinaZaDatum($datum){
        return count(Projekat::where('poslata', 1)->whereDate('updated_at', "$datum")->get());
    }

    public static function dohvatiIznoseZaDatume($datumOd, $datumDo){
        return DB::select("
            select AVG(p.budzet) as prosek, MAX(p.budzet) maksimalna
            from projekat p 
            where p.poslata = 1
            and DATE(p.updated_at) >= '$datumOd'
            and DATE(p.updated_at) < '$datumDo'
        ")[0];
    }

}
