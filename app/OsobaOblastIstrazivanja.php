<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsobaOblastIstrazivanja extends Model
{
    protected $table = 'osoba_oblast_istrazivanja';
    protected $fillable = ['id_osoba', 'id_oblast_istrazivanja'];

    public static function dohvatiZaOsobu($id_osoba){
        return OsobaOblastIstrazivanja::where('id_osoba', $id_osoba)->get();
    }

    public function napuni($id_osoba, $id_oblast_istrazivanja){
        $this->id_osoba = $id_osoba;
        $this->id_oblast_istrazivanja = $id_oblast_istrazivanja;

        $this->save();
    }

    public function obrisi(){
        OsobaOblastIstrazivanja::where('id_osoba', $this->id_osoba)->where('id_oblast_istrazivanja', $this->id_oblast_istrazivanja)->delete();
    }
}
