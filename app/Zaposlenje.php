<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zaposlenje extends Model
{
    protected $table = 'zaposlenje';
    protected $fillable = ['datum_pocetka', 'datum_kraja', 'id_naucna_ustanova', 'pozicija'];

    protected $appends = ['naucnaUstanova'];

    private $naucnaUstanova;

    public function getNaucnaUstanovaAttribute()
    {
        return $this->naucnaUstanova;
    }

    public function setNaucnaUstanovaAttribute($naucnaUstanova)
    {
        $this->naucnaUstanova = $naucnaUstanova;
    }

    public static function dohvatiSaId($id){
        return Zaposlenje::where('id', $id)->first();
    }

    public function napuni($datum_pocetka, $datum_kraja, $id_naucna_ustanova, $pozicija){
        $this->datum_pocetka = $datum_pocetka;
        $this->datum_kraja = $datum_kraja;
        $this->id_naucna_ustanova = $id_naucna_ustanova;
        $this->pozicija = $pozicija;

        $this->save();
    }

    public function obrisi(){
        $this->delete();
    }
}
