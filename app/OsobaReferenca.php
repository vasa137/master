<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsobaReferenca extends Model
{
    protected $table = 'osoba_referenca';
    protected $fillable = ['id_osoba', 'id_referenca'];

    public static function dohvatiZaOsobu($id_osoba){
        return OsobaReferenca::where('id_osoba', $id_osoba)->get();
    }

    public function napuni($id_osoba, $id_referenca){
        $this->id_osoba = $id_osoba;
        $this->id_referenca = $id_referenca;

        $this->save();
    }

    public function obrisi(){
        OsobaReferenca::where('id_osoba', $this->id_osoba)->where('id_referenca', $this->id_referenca)->delete();
    }

}
