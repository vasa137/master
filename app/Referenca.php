<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referenca extends Model
{
    protected $table = 'referenca';
    protected $fillable = ['identifikator', 'naziv_rada', 'id_kategorija_rada', 'mesta_objavljivanja', 'potpisnici'];

    protected $appends = ['kategorijaRada'];

    private $kategorijaRada;

    public function getKategorijaRadaAttribute()
    {
        return $this->kategorijaRada;
    }

    public function setKategorijaRadaAttribute($kategorijaRada)
    {
        $this->kategorijaRada = $kategorijaRada;
    }

    public static function dohvatiSaId($id){
        return Referenca::where('id', $id)->first();
    }

    public function napuni($identifikator, $naziv_rada, $id_kategorija_rada, $mesta_objavljivanja, $potpisnici){
        $this->identifikator = $identifikator;
        $this->naziv_rada = $naziv_rada;
        $this->id_kategorija_rada = $id_kategorija_rada;
        $this->mesta_objavljivanja = $mesta_objavljivanja;
        $this->potpisnici = $potpisnici;

        $this->save();
    }

    public function obrisi(){
        $this->delete();
    }

}
