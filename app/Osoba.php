<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Osoba extends Model
{
    protected $table = 'osoba';
    protected $fillable = ['ime', 'prezime', 'datum_rodjenja'];

    protected $appends = ['oblastiIstrazivanja', 'obrazovanja', 'zaposlenja', 'reference', 'trenutnaNaucnaUstanova'];

    private $oblastiIstrazivanja;
    private $obrazovanja;
    private $zaposlenja;
    private $reference;
    private $trenutnaNaucnaUstanova;

    public function getObrazovanjaAttribute()
    {
        return $this->obrazovanja;
    }

    public function setObrazovanjaAttribute($obrazovanja)
    {
        $this->obrazovanja = $obrazovanja;
    }

    public function getZaposlenjaAttribute()
    {
        return $this->zaposlenja;
    }

    public function setZaposlenjaAttribute($zaposlenja)
    {
        $this->zaposlenja = $zaposlenja;
    }

    public function getReferenceAttribute()
    {
        return $this->reference;
    }

    public function setReferenceAttribute($reference)
    {
        $this->reference = $reference;
    }

    public static function dohvatiSaId($id){
        return Osoba::where('id', $id)->first();
    }

    public function getOblastiIstrazivanjaAttribute()
    {
        return $this->oblastiIstrazivanja;
    }

    public function setOblastiIstrazivanjaAttribute($oblastiIstrazivanja)
    {
        $this->oblastiIstrazivanja = $oblastiIstrazivanja;
    }


    public function getTrenutnaNaucnaUstanovaAttribute()
    {
        return $this->trenutnaNaucnaUstanova;
    }

    public function setTrenutnaNaucnaUstanovaAttribute($trenutnaNaucnaUstanova)
    {
        $this->trenutnaNaucnaUstanova = $trenutnaNaucnaUstanova;
    }

    public function napuni($ime, $prezime, $datum_rodjenja){
        $this->ime = $ime;
        $this->prezime = $prezime;
        $this->datum_rodjenja = $datum_rodjenja;

        $this->save();
    }

    public function obrisi(){
        $this->delete();
    }

}
