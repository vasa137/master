<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsobaProjekat extends Model
{
    protected $table = 'osoba_projekat';
    protected $fillable = ['id_osoba', 'id_projekat'];

    public function napuni($id_osoba, $id_projekat){
        $this->id_osoba = $id_osoba;
        $this->id_projekat = $id_projekat;

        $this->save();
    }

    public static function dohvatiZaProjekat($id_projekat){
        return OsobaProjekat::where('id_projekat', $id_projekat)->get();
    }

    public function obrisi(){
        OsobaProjekat::where('id_osoba', $this->id_osoba)->where('id_projekat', $this->id_projekat)->delete();
    }
}
