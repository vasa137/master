<?php

namespace App;

use App\Notifications\ZaboravljenaLozinkaNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','password','remember_token','admin', 'blokiran', 'ime_prezime'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ZaboravljenaLozinkaNotification($token));
    }

    public static function dohvatiSaEmailom($email){
        return User::where('email', $email)->first();
    }

    public static function dohvatiSveKojiNisuAdmini(){
        return User::where('admin', 0)->get();
    }

    public static function dohvatiSaId($id){
        return User::where('id', $id)->first();
    }

    public static function dohvatiNeblokiraneKojiNisuAdmini(){
        return User::where('admin',0)->where('blokiran', 0)->get();
    }

    public static function dohvatiBlokirane(){
        return User::where('blokiran', 1)->get();
    }

    public function blokiraj(){
        $this->blokiran = 1;
        $this->save();
    }

    public function odblokiraj(){
        $this->blokiran = 0;
        $this->save();
    }
}
