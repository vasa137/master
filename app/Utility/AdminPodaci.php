<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 01-Aug-19
 * Time: 13:42
 */

namespace App\Utility;


class AdminPodaci
{
    public  $oblastiProjekta;

    public  $oblastiIstrazivanja;

    public  $stepeniStudija;

    public  $naucneUstanove;

    public  $obrazovneUstanove;

    public  $kategorijeRada;

    public function __construct($oblastiProjekta, $oblastiIstrazivanja, $stepeniStudija, $naucneUstanove, $obrazovneUstanove, $kategorijeRada)
    {
        $this->oblastiProjekta = $oblastiProjekta;
        $this->oblastiIstrazivanja = $oblastiIstrazivanja;
        $this->stepeniStudija = $stepeniStudija;
        $this->naucneUstanove = $naucneUstanove;
        $this->obrazovneUstanove = $obrazovneUstanove;
        $this->kategorijeRada = $kategorijeRada;
    }


}