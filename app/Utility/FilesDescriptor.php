<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 01-Aug-19
 * Time: 18:47
 */

namespace App\Utility;


class FilesDescriptor
{
    public $projectFiles;
    public $bibliography;
    public $biography;

    public function __construct($projectFiles, $bibliography, $biography)
    {
        $this->projectFiles = $projectFiles;
        $this->bibliography = $bibliography;
        $this->biography = $biography;
    }


}