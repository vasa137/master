/*
 *  Document   : be_pages_ecom_dashboard.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in e-Commerce Dashboard Page
 */
let datumi;
let brojeviPorudzbina;
let maxBroj = 0;
let iznosi;
let maxIznos = 0;
var BePagesEcomDashboard = function() {
    // Chart.js Charts, for more examples you can check out http://www.chartjs.org/docs
    var initEcomChartJS = function () {
        // Set Global Chart.js configuration
        Chart.defaults.global.defaultFontColor              = '#555555';
        Chart.defaults.scale.gridLines.color                = "transparent";
        Chart.defaults.scale.gridLines.zeroLineColor        = "transparent";
        Chart.defaults.scale.display                        = false;
        Chart.defaults.scale.ticks.beginAtZero              = true;
        Chart.defaults.scale.ticks.suggestedMax             = 1.25*maxIznos;
        Chart.defaults.global.elements.line.borderWidth     = 2;
        Chart.defaults.global.elements.point.radius         = 5;
        Chart.defaults.global.elements.point.hoverRadius    = 7;
        Chart.defaults.global.tooltips.cornerRadius         = 3;
        Chart.defaults.global.legend.display                = false;

        // Chart Containers
        var chartEcomEarningsCon    = jQuery('.js-chartjs-ecom-dashboard-earnings');
        var chartEcomOrdersCon      = jQuery('.js-chartjs-ecom-dashboard-orders');

        // Charts Variables
        var chartEcomOrders, chartEcomEarnings;

        // Charts Data
        var chartEcomEarningsData = {
            labels: datumi,
            datasets: [
                {
                    label: 'Укупан износ',
                    fill: true,
                    backgroundColor: 'rgba(188,38,211,.25)',
                    borderColor: 'rgba(188,38,211,1)',
                    pointBackgroundColor: 'rgba(188,38,211,1)',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: 'rgba(188,38,211,1)',
                    data: iznosi
                }
            ]
        };

        var chartEcomOrdersData = {
            labels: datumi,
            datasets: [
                {
                    label: 'Број пројеката',
                    fill: true,
                    backgroundColor: 'rgba(112,178,156,.25)',
                    borderColor: 'rgba(112,178,156,1)',
                    pointBackgroundColor: 'rgba(112,178,156,1)',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: 'rgba(112,178,156,1)',
                    data: brojeviPorudzbina
                }
            ]
        };

        // Init Charts
        if (chartEcomEarningsCon.length ) {
            chartEcomEarnings = new Chart(chartEcomEarningsCon, { type: 'line', data: chartEcomEarningsData, options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel;
                        }
                    }

                }
            }});
        }

        if (chartEcomOrdersCon.length ) {
            Chart.defaults.scale.ticks.suggestedMax = 1.25*maxBroj;

            chartEcomOrders  = new Chart(chartEcomOrdersCon, { type: 'line', data: chartEcomOrdersData});
        }

    };

    return {
        init: function () {
            // Init Chart.js Charts
            initEcomChartJS();
        }
    };
}();

function inicijalizujKontrolnuTablu(nizDatuma, nizBrojevaPorudzbina, nizIznosi){
    datumi = JSON.parse(nizDatuma);
    brojeviPorudzbina = JSON.parse(nizBrojevaPorudzbina);

    for(let i = 0; i < brojeviPorudzbina.length; i++){
        if(parseInt(brojeviPorudzbina[i]) > maxBroj){
            maxBroj = parseInt(brojeviPorudzbina[i]);
        }
    }

    iznosi = JSON.parse(nizIznosi);

    for(let i = 0; i < iznosi.length; i++){
        if(parseInt(iznosi[i]) > maxIznos){
            maxIznos = parseInt(iznosi[i]);
        }
    }

    BePagesEcomDashboard.init();
}