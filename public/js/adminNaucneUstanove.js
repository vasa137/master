function showAvailable(){
    $('#tabela-naucne_ustanove-obrisani').css('display', 'none');
    $('#tabela-naucne_ustanove-aktivni').css('display', 'block');
    $('#tabela-naucne_ustanove-obrisani_wrapper').css('display', 'none');
    $('#tabela-naucne_ustanove-aktivni_wrapper').css('display', 'block');

    $('#naucne_ustanove-title').html('Доступне научно-истраживачке установе');
}

function showUnavailable(){
    $('#tabela-naucne_ustanove-obrisani').css('display', 'block');
    $('#tabela-naucne_ustanove-aktivni').css('display', 'none');
    $('#tabela-naucne_ustanove-obrisani_wrapper').css('display', 'block');
    $('#tabela-naucne_ustanove-aktivni_wrapper').css('display', 'none');

    $('#naucne_ustanove-title').html('Обрисане научно-истраживачке установе');
}