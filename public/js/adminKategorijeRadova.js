function showAvailable(){
    $('#tabela-kategorije_radova-obrisani').css('display', 'none');
    $('#tabela-kategorije_radova-aktivni').css('display', 'block');
    $('#tabela-kategorije_radova-obrisani_wrapper').css('display', 'none');
    $('#tabela-kategorije_radova-aktivni_wrapper').css('display', 'block');

    $('#kategorije_radova-title').html('Доступне категорије научних радова');
}

function showUnavailable(){
    $('#tabela-kategorije_radova-obrisani').css('display', 'block');
    $('#tabela-kategorije_radova-aktivni').css('display', 'none');
    $('#tabela-kategorije_radova-obrisani_wrapper').css('display', 'block');
    $('#tabela-kategorije_radova-aktivni_wrapper').css('display', 'none');

    $('#kategorije_radova-title').html('Обрисане категорије научних радова');
}