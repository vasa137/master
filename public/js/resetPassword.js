function checkResetData(){
    $email = $('#reset-email').val();
    $password = $('#reset-password').val();
    $confirmPassword = $('#reset-confirm-password').val();

    $('.reset-error').hide();

    let error = false;

    if($email == ''){
        $('#reset-email-required').show();
        error = true;
    }

    if($email.length > 254){
        $('#reset-email-maxlen').show();
        error = true;
    }

    if(!error){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(String($email).toLowerCase())){
            $('#reset-email-format').show();
            error = true;
        }
    }

    if($password == ''){
        $('#reset-password-required').show();
        error = true;
    } else if($password.length < 6){
        $('#reset-password-minlen').show();
        error = true;
    }

    if($password.length > 254){
        $('#reset-password-maxlen').show();
        error = true;
    }

    if($confirmPassword == ''){
        $('#reset-confirm-password-required').show();
        error = true;
    } else if($password != $confirmPassword){
        $('#reset-confirm-password-equal').show();
        error = true;
    }

    return !error;
}

function checkEmailResetData(){
    $email = $('#reset-email').val();

    $('.reset-error').hide();

    let error = false;

    if($email == ''){
        $('#reset-email-required').show();
        error = true;
    }

    if($email.length > 254){
        $('#reset-email-maxlen').show();
        error = true;
    }

    if(!error){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(String($email).toLowerCase())){
            $('#reset-email-format').show();
            error = true;
        }
    }

    return !error;
}