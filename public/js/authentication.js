$(document).ready(function(){
    $("#bt1").click(function(){
        if ($('.rotate').length > 0) {
            $("#iwp").removeClass("rotate");
        }
        if ($('.rotateb').length > 0) {
            $("#iwp").removeClass("rotateb");

        }
        $("#iwp").addClass("rotate");
        $('.front').fadeOut("normal", function () {
            // your other code
            $('.back').show();
        });

    });
    $("#bt2").click(function(){

        if ($('.rotate').length > 0) {
            $("#iwp").removeClass("rotate");
        }
        if ($('.rotateb').length > 0) {
            $("#iwp").removeClass("rotateb");
        }
        $("#iwp").addClass("rotateb");
        $('.back').fadeOut("normal", function () {
            // your other code
            $('.front').show();
        });
    });
});

function checkLoginData(){
    $email = $('#login-email').val();
    $password = $('#login-password').val();

    $('.login-error').hide();

    let error = false;

    if($email == ''){
        $('#login-email-required').show();
        error = true;
    }

    if($email.length > 254){
        $('#login-email-maxlen').show();
        error = true;
    }

    if(!error){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(String($email).toLowerCase())){
            $('#login-email-format').show();
            error = true;
        }
    }

    if($password == ''){
        $('#login-password-required').show();
        error = true;
    }

    if($password.length > 254){
        $('#login-password-maxlen').show();
        error = true;
    }

    return !error;
}
function checkRegisterData(){
    $name = $('#register-name').val();
    $email = $('#register-email').val();
    $password = $('#register-password').val();
    $confirmPassword = $('#register-confirm-password').val();

    $('.register-error').hide();

    let error = false;

    if($name == ''){
        $('#register-name-required').show();
        error = true;
    }

    if($name.length > 254){
        $('#register-name-maxlen').show();
        error = true;
    }

    if($email == ''){
        $('#register-email-required').show();
        error = true;
    }

    if($email.length > 254){
        $('#register-email-maxlen').show();
        error = true;
    }

    if(!error){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(String($email).toLowerCase())){
            $('#register-email-format').show();
            error = true;
        }
    }

    if($password == ''){
        $('#register-password-required').show();
        error = true;
    } else if($password.length < 6){
        $('#register-password-minlen').show();
        error = true;
    }

    if($password.length > 254){
        $('#register-password-maxlen').show();
        error = true;
    }

    if($confirmPassword == ''){
        $('#register-confirm-password-required').show();
        error = true;
    } else if($password != $confirmPassword){
        $('#register-confirm-password-equal').show();
        error = true;
    }

    return !error;
}
