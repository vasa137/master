function showAvailable(){
    $('#tabela-oblasti_istrazivanja-obrisani').css('display', 'none');
    $('#tabela-oblasti_istrazivanja-aktivni').css('display', 'block');
    $('#tabela-oblasti_istrazivanja-obrisani_wrapper').css('display', 'none');
    $('#tabela-oblasti_istrazivanja-aktivni_wrapper').css('display', 'block');

    $('#oblasti_istrazivanja-title').html('Доступне области истраживања');
}

function showUnavailable(){
    $('#tabela-oblasti_istrazivanja-obrisani').css('display', 'block');
    $('#tabela-oblasti_istrazivanja-aktivni').css('display', 'none');
    $('#tabela-oblasti_istrazivanja-obrisani_wrapper').css('display', 'block');
    $('#tabela-oblasti_istrazivanja-aktivni_wrapper').css('display', 'none');

    $('#oblasti_istrazivanja-title').html('Обрисане области истраживања');
}