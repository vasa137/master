function showAvailable(){
    $('#tabela-oblasti_projekata-obrisani').css('display', 'none');
    $('#tabela-oblasti_projekata-aktivni').css('display', 'block');
    $('#tabela-oblasti_projekata-obrisani_wrapper').css('display', 'none');
    $('#tabela-oblasti_projekata-aktivni_wrapper').css('display', 'block');

    $('#oblasti_projekata-title').html('Доступне области пројеката');
}

function showUnavailable(){
    $('#tabela-oblasti_projekata-obrisani').css('display', 'block');
    $('#tabela-oblasti_projekata-aktivni').css('display', 'none');
    $('#tabela-oblasti_projekata-obrisani_wrapper').css('display', 'block');
    $('#tabela-oblasti_projekata-aktivni_wrapper').css('display', 'none');

    $('#oblasti_projekata-title').html('Обрисане области пројеката');
}