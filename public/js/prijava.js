$modalEducation = $('#modal-education');
$modalJob = $('#modal-job');
$modalReference = $('#modal-reference');
$modalDeleteEducation = $('#modal-delete-education');
$modalDeleteJob = $('#modal-delete-job');
$modalDeleteReference = $('#modal-delete-reference');
$modalSaveSuccessful = $('#modal-save-successful');
$modalSaveFailed = $('#modal-save-failed');
$modalLoader = $('#modal-loader');
let page = 1;
let edit = false;
let submitted = false;
const NEW_EDUCATION = -1, NEW_JOB = -1, NEW_REFERENCE = -1, MAX_REFERENCES = 5, MAX_TEAM = 5, PROJECT_FILES_INPUTS = 5, MAX_RESEARCH_AREA_SELECTS = 5, MAX_STATEMENT_FILES = 6;

function Education (id, universityName, universityId, studiesType, studiesTypeId, universityTitle, graduationDate, peopleId) {
    this.id = id;
    this.peopleId = peopleId;
    this.changeData = function (universityName, universityId, studiesType, studiesTypeId, universityTitle, graduationDate) { // it can access private members
        this.universityName = universityName;
        this.universityId = universityId;
        this.studiesType = studiesType;
        this.studiesTypeId = studiesTypeId;
        this.universityTitle = universityTitle;
        this.graduationDate = graduationDate;
    };

    this.changeData(universityName, universityId, studiesType, studiesTypeId, universityTitle, graduationDate);
}

function Job (id, institutionName, institutionId, jobPosition,  jobStartDate, jobEndDate, peopleId) {
    this.id = id;
    this.peopleId = peopleId;
    this.changeData = function (institutionName, institutionId, jobPosition,  jobStartDate, jobEndDate) { // it can access private members
        this.institutionName = institutionName;
        this.institutionId = institutionId;
        this.jobPosition = jobPosition;
        this.jobStartDate = jobStartDate;
        this.jobEndDate = jobEndDate;
    };

    this.changeData(institutionName, institutionId, jobPosition,  jobStartDate, jobEndDate);
}

function Reference (id, referenceWorkCategory, referenceWorkCategoryId, referenceWorkTitle, referenceWorkIdentifier,  referenceWorkMagazines, referenceWorkSigners, peopleId) {
    this.id = id;
    this.peopleId = peopleId;
    this.changeData = function (referenceWorkCategory, referenceWorkCategoryId, referenceWorkTitle, referenceWorkIdentifier,  referenceWorkMagazines, referenceWorkSigners) { // it can access private members
        this.referenceWorkCategory = referenceWorkCategory;
        this.referenceWorkCategoryId = referenceWorkCategoryId;
        this.referenceWorkTitle = referenceWorkTitle;
        this.referenceWorkIdentifier = referenceWorkIdentifier;
        this.referenceWorkMagazines = referenceWorkMagazines;
        this.referenceWorkSigners = referenceWorkSigners;
    };

    this.changeData(referenceWorkCategory, referenceWorkCategoryId, referenceWorkTitle, referenceWorkIdentifier,  referenceWorkMagazines, referenceWorkSigners);
}

function Project(title, abstract, id_area_project, budget){
    this.title = title;
    this.abstract = abstract;
    this.id_area_project = id_area_project;
    this.budget = budget;

    this.members = [];
    this.filesDescriptor = null;

    // KORISTI SE SAMO KOD FINALNOG SLANJA
    this.conditionsAgreed = false;

    this.agreeConditions = function(){
        this.conditionsAgreed = true;
    };

    this.addPerson = function(person){
        this.members[this.members.length] = person;
    };

    this.setFilesDescriptor = function(filesDescriptor){
        this.filesDescriptor = filesDescriptor;
    }

}

function Person(firstName, lastName, birthDate, id_area_research){
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.id_area_research = id_area_research;

    this.educations = [];
    this.jobs = [];
    this.references = [];

    this.addEducation = function(education){
        this.educations[this.educations.length] = education;
    };

    this.addJob = function(job){
        this.jobs[this.jobs.length] = job;
    };

    this.addReference = function(reference){
        this.references[this.references.length] = reference;
    };
}

function ServerEducation(id_education_institution, id_studies_degree, title, date){
    this.id_education_institution = id_education_institution;
    this.id_studies_degree = id_studies_degree;
    this.title = title;
    this.date = date;
}

function ServerJob(date_start, date_end, id_scientific_institution, position){
    this.date_start = date_start;
    this.date_end = date_end;
    this.id_scientific_institution = id_scientific_institution;
    this.position = position;
}

function ServerReference(identifier, title, id_category_reference, announcement_places, signers){
    this.identifier = identifier;
    this.title = title;
    this.id_category_reference = id_category_reference;
    this.announcement_places = announcement_places;
    this.signers = signers;
}

function FilesDescriptor(projectFilesFlags, statementFilesCount, bibliographyFlags, biographyFlags){
    this.projectFilesFlags = projectFilesFlags;
    this.statementFilesCount = statementFilesCount;
    this.bibliographyFlags = bibliographyFlags;
    this.biographyFlags = biographyFlags;
}

let teamNum = 1;

let educations = [];
let jobs = [];
let references = [];

let peopleEducations = [];
let peopleJobs = [];
let peopleReferences = [];

let memberFlags = [];

let membersResearchAreaLastValue = [];

let projectFilesFlags = [];

let statementFilesCount = 0;

let bibliographyFlags = [];
let bibliographyFileNames = []; // kada se odstiklira i stiklira ucesnik da sacuvamo naziv fajla u textboxu

let biographyFlags = [];
let biographyFileNames = []; // kada se odstiklira i stiklira ucesnik da sacuvamo naziv fajla u textboxu

let educationIdCounter = 1;
let jobIdCounter = 1;
let referenceIdCounter = 1;

function initVariables(){
    for(let i = 0; i <= MAX_TEAM; i++){
        peopleEducations[i] = [];
        peopleJobs[i] = [];
        peopleReferences[i] = [];
        biographyFlags[i] = false;
        biographyFileNames[i] = '';

        bibliographyFlags[i] = false;
        bibliographyFileNames[i] = '';

        membersResearchAreaLastValue[i] = [];

        if(i >= 1){
            memberFlags[i] = false;
            $memberContent = $('#member-' + i + '-content');
            $memberContent.hide();
        } else{
            memberFlags[i] = true;
        }
    }

    for(let i = 0; i < PROJECT_FILES_INPUTS; i++){
        projectFilesFlags[i] = false;
    }
}

function initEditProject(projekat){
    projekat = JSON.parse(projekat);

    edit = true;

    if(projekat.poslata){
        submitted = true;
    }

    initVariables();

    teamNum = projekat.osobe.length;

    for(let i = 0; i < projekat.osobe.length; i++){
        let osoba = projekat.osobe[i];

        memberFlags[i] = true;

        $researchArea = $('#' + i + "-researchArea");

        membersResearchAreaLastValue[i] = $researchArea.val();

        $educationCollection = $('#educationCollection-' + i);

        for(let j = 0; j < osoba.obrazovanja.length; j++){
            let obrazovanje = osoba.obrazovanja[j];
            let education = new Education (educationIdCounter, obrazovanje.obrazovnaUstanova.naziv, obrazovanje.obrazovnaUstanova.id, obrazovanje.stepenStudija.naziv, obrazovanje.stepenStudija.id, obrazovanje.zvanje, dateLocalFormat(obrazovanje.datum) , i);
            educationIdCounter++;

            educations.push(education);
            peopleEducations[i].push(education);

            $educationCollection.append(educationListElem(education));
        }

        $jobCollection = $('#jobCollection-' + i);

        for(let j = 0; j < osoba.zaposlenja.length; j++){
            let zaposlenje = osoba.zaposlenja[j];
            console.log(zaposlenje);
            let job = new Job (jobIdCounter, zaposlenje.naucnaUstanova.naziv, zaposlenje.naucnaUstanova.id, zaposlenje.pozicija,   dateLocalFormat(zaposlenje.datum_pocetka), zaposlenje.datum_kraja != null ? dateLocalFormat(zaposlenje.datum_kraja) : '', i);
            jobIdCounter++;

            jobs.push(job);
            peopleJobs[i].push(job);

            $jobCollection.append(jobListElem(job));
        }

        $referenceCollection = $('#referenceCollection-' + i);

        for(let j = 0; j < osoba.reference.length; j++){
            let referenca = osoba.reference[j];
            let reference = new Reference (referenceIdCounter, referenca.kategorijaRada.naziv, referenca.kategorijaRada.id, referenca.naziv_rada, referenca.identifikator,  referenca.mesta_objavljivanja, referenca.potpisnici, i);
            referenceIdCounter++;

            references.push(reference);
            peopleReferences[i].push(reference);

            $referenceCollection.append(referenceListElem(reference));
        }

        if(peopleReferences[i].length == MAX_REFERENCES){
            $('#' + i + '-reference-add-button').addClass('disabled');
        }

        let bibliographyArray = projekat.filesDescriptor.bibliography[i];

        if(bibliographyArray.length == 0){
            bibliographyFlags[i] = false;
            bibliographyFileNames[i] = '';
        } else{
            bibliographyFlags[i] = true;
            bibliographyFileNames[i] = bibliographyArray[0];
        }

        let biographyArray = projekat.filesDescriptor.biography[i];

        if(biographyArray.length == 0){
            biographyFlags[i] = false;
            biographyFileNames[i] = '';
        } else{
            biographyFlags[i] = true;
            biographyFileNames[i] = biographyArray[0];
        }

        $memberCheckbox = $('#member-' + i + '-CB');

        $memberCheckbox.click();
    }

    for(let i = 0; i < projekat.filesDescriptor.projectFiles.length; i++){
        let projectFilesArray = projekat.filesDescriptor.projectFiles[i];

        if(projectFilesArray.length == 0){
            projectFilesFlags[i] = false;

        } else{
            projectFilesFlags[i] = true;
        }
    }

    statementFilesCount = projekat.filesDescriptor.projectFiles[4].length;

    for(let i = projekat.osobe.length; i <= MAX_TEAM; i++){
        $('#member-tabheading-' + i).hide();
    }

    if(projekat.poslata){
        $('input').attr("disabled", true);
        $('select').attr("disabled", true);
        $('textarea').attr("disabled", true);
        $('.add-button').attr("disabled", true);
    }
}

$(document).ready(function() {
    $('input#input_text, textarea#abstractTA').characterCounter();
    $('.tooltipped').tooltip();
    $('select').formSelect();
    $('.collapsible').collapsible();
    $('.modal').modal({
        dismissible: false
    });
    $('.tabs').tabs();
    $('.datepicker').datepicker({
        format: 'dd.mm.yyyy.',
        yearRange: 70,
        i18n :
                {
                    cancel:
                    'Откажи',
                    clear:
                    'Поништи',
                    done:
                    'Потврди',
                    months:
                        [
                            'Јануар',
                            'Фебруар',
                            'Март',
                            'Април',
                            'Мај',
                            'Јун',
                            'Јул',
                            'Август',
                            'Септембар',
                            'Октобар',
                            'Новембар',
                            'Децембар'
                        ],

                    monthsShort:
                        [
                            'Јан',
                            'Феб',
                            'Мар',
                            'Апр',
                            'Мај',
                            'Јун',
                            'Јул',
                            'Авг',
                            'Сеп',
                            'Окт',
                            'Нов',
                            'Дец'
                        ],

                    weekdays:
                        [
                            'Недеља',
                            'Понедељак',
                            'Уторак',
                            'Среда',
                            'Четвртак',
                            'Петак',
                            'Субота',

                        ],

                    weekdaysShort:
                        [
                            'Нед',
                            'Пон',
                            'Уто',
                            'Сре',
                            'Чет',
                            'Пет',
                            'Суб'
                        ],

                    weekdaysAbbrev:    ['Н', 'П', 'У', 'С', 'Ч', 'П', 'С']
                }

    });

    $("#budgetText").on("keyup", function(event ) {
        // When user select text in the document, also abort.
        var selection = window.getSelection().toString();
        if (selection !== '') {
            return;
        }
        // When the arrow keys are pressed, abort.
        if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
            return;
        }
        var $this = $(this);
        // Get the value.
        var input = $this.val();
        input = input.replace(/,/g, "");

        input = input?parseInt(input, 10):0;

        if(isNaN(input)){
            input = 0;
        }

        if(input > 200000){
            input = 200000;
        }
        $this.val(function () {
            return (input === 0)?"":input.toLocaleString("en-GB");
        });
    });

    if(!edit) {
        initVariables();
    }
});

function educationDataContent(education){
    return education.graduationDate + ' - <strong>' + education.universityName + '</strong> - ' + education.studiesType;
}

function educationListElem(education){
    if(!submitted) {
        return '<li class="collection-item" id="education-elem-' + education.id + '"><div><span id="education-data-' + education.id + '">' + educationDataContent(education) + '</span><a href="javascript: deleteEducationDialog(' + education.id + ')" class="secondary-content delete-content tooltipped" data-position="right" data-tooltip="Обриши податке о образовању"><i class="material-icons">delete</i></a>&emsp;<a href="javascript:editEducation(' + education.id + ')" class="secondary-content edit-content tooltipped" data-position="right" data-tooltip="Измени податке о образовању"><i class="material-icons">edit</i></a></div></li>';
    }else{
        return '<li class="collection-item" id="education-elem-' + education.id + '"><div><span id="education-data-' + education.id + '">' + educationDataContent(education) + '</span><a href="javascript:editEducation(' + education.id + ')" class="secondary-content edit-content tooltipped" data-position="right" data-tooltip="Погледај податке о образовању"><i class="material-icons">remove_red_eye</i></a></div></li>';
    }
}

function jobDataContent(job){
    if(job.jobEndDate == ''){
        return job.jobStartDate + ' -> тренутно - <strong>' + job.institutionName + '</strong>';
    } else {
        return job.jobStartDate + ' -> ' + job.jobEndDate + ' - <strong>' + job.institutionName + '</strong>';
    }
}

function jobListElem(job){
    if(!submitted) {
        return '<li class="collection-item" id="job-elem-' + job.id + '"><div><span id="job-data-' + job.id + '">' + jobDataContent(job) + '</span><a href="javascript: deleteJobDialog(' + job.id + ')" class="secondary-content delete-content tooltipped" data-position="right" data-tooltip="Обриши податке о запослењу"><i class="material-icons">delete</i></a>&emsp;<a href="javascript:editJob(' + job.id + ')" class="secondary-content edit-content tooltipped" data-position="right" data-tooltip="Измени податке о запослењу"><i class="material-icons">edit</i></a></div></li>';
    } else{
        return '<li class="collection-item" id="job-elem-' + job.id + '"><div><span id="job-data-' + job.id + '">' + jobDataContent(job) + '</span><a href="javascript:editJob(' + job.id + ')" class="secondary-content edit-content tooltipped" data-position="right" data-tooltip="Погледај податке о запослењу"><i class="material-icons">remove_red_eye</i></a></div></li>';
    }
}

function referenceDataContent(reference){
    return '<strong>Идентификатор: </strong>' + reference.referenceWorkIdentifier + ' - <strong>Назив: </strong>' + reference.referenceWorkTitle + ' - <strong>Часописи: </strong>' + reference.referenceWorkMagazines;
}

function referenceListElem(reference){
    if(!submitted) {
        return '<li class="collection-item" id="reference-elem-' + reference.id + '"><div><span id="reference-data-' + reference.id + '">' + referenceDataContent(reference) + '</span><a href="javascript: deleteReferenceDialog(' + reference.id + ')" class="secondary-content delete-content tooltipped" data-position="right" data-tooltip="Обриши референцу"><i class="material-icons">delete</i></a>&emsp;<a href="javascript:editReference(' + reference.id + ')" class="secondary-content edit-content tooltipped" data-position="right" data-tooltip="Измени референцу"><i class="material-icons">edit</i></a></div></li>';
    } else{
        return '<li class="collection-item" id="reference-elem-' + reference.id + '"><div><span id="reference-data-' + reference.id + '">' + referenceDataContent(reference) + '</span><a href="javascript:editReference(' + reference.id + ')" class="secondary-content edit-content tooltipped" data-position="right" data-tooltip="Погледај референцу"><i class="material-icons">remove_red_eye</i></a></div></li>';
    }
}

function findEducation(idEducation){
    for(let i = 0; i < educations.length; i++){
        if(educations[i].id == idEducation){
            return educations[i];
        }
    }
}

function removeEducation(idEducation){
    let index;
    let education;
    for(let i = 0; i < educations.length; i++){
        if(educations[i].id == idEducation){
            index = i;
            education = educations[i];
            break;
        }
    }

    educations.splice(index, 1);

    for(let i = 0; i < peopleEducations[education.peopleId].length; i++){
        if(peopleEducations[education.peopleId][i].id == idEducation){
            index = i;
            break;
        }
    }

    peopleEducations[education.peopleId].splice(index, 1);

    return education;
}

function findJob(idJob){
    for(let i = 0; i < jobs.length; i++){
        if(jobs[i].id == idJob){
            return jobs[i];
        }
    }
}

function removeJob(idJob){
    let index;
    let job;
    for(let i = 0; i < jobs.length; i++){
        if(jobs[i].id == idJob){
            index = i;
            job = jobs[i];
            break;
        }
    }

    jobs.splice(index, 1);

    for(let i = 0; i < peopleJobs[job.peopleId].length; i++){
        if(peopleJobs[job.peopleId][i].id == idJob){
            index = i;
            break;
        }
    }

    peopleJobs[job.peopleId].splice(index, 1);

    return job;
}

function findReference(idReference){
    for(let i = 0; i < references.length; i++){
        if(references[i].id == idReference){
            return references[i];
        }
    }
}

function removeReference(idReference){
    let index;
    let reference;
    for(let i = 0; i < references.length; i++){
        if(references[i].id == idReference){
            index = i;
            reference = references[i];
            break;
        }
    }

    references.splice(index, 1);

    for(let i = 0; i < peopleReferences[reference.peopleId].length; i++){
        if(peopleReferences[reference.peopleId][i].id == idReference){
            index = i;
            break;
        }
    }

    peopleReferences[reference.peopleId].splice(index, 1);

    return reference;
}

function convertToDateObject(date){
    let dateParts = date.split(".");

    // month is 0-based, that's why we need dataParts[1] - 1
    return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
}

function dateSqlFormat(date){
    let dateParts = date.split(".");

    return dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];
}

function dateLocalFormat(date){
    let dateParts = date.split("-");

    return dateParts[2] + '.' + dateParts[1] + '.' + dateParts[0];
}

function checkEducationInput(universityName, studiesType, universityTitle, graduationDate){
    let result = true;

    $('.education-error').hide();

    if(universityName == ''){
        $('#universityNameSelect-required').show();
        result = false;
    }

    if(studiesType == ''){
        $('#studiesTypeSelect-required').show();
        result = false;
    }

    if(universityTitle == ''){
        $('#universityTitle-required').show();
        result = false;
    }

    if(universityTitle.length > 255){
        $('#universityTitle-maxlen').show();
        result = false;
    }

    if(graduationDate == ''){
        $('#graduationDate-required').show();
        result = false;
    } else {
        let daysPassed = moment([]).diff(convertToDateObject(graduationDate).toString(), "days");

        if (daysPassed < 0) {
            $('#graduationDate-future').show();
            result = false;
        }
    }

    return result;
}

function saveEducation(){
    $universityNameSelect = $modalEducation.find('#universityNameSelect');
    $studiesTypeSelect = $modalEducation.find('#studiesTypeSelect');
    $universityTitle = $modalEducation.find('#universityTitle').val().trim();
    $graduationDate = $modalEducation.find('#graduationDate').val();
    $inputPeopleId = $modalEducation.find('#peopleId');
    $inputEducationId = $modalEducation.find("#educationId");

    $universityNameText = $universityNameSelect.children("option:selected").text();
    $studiesTypeText = $studiesTypeSelect.children("option:selected").text();

    let checkResult = checkEducationInput($universityNameText, $studiesTypeText, $universityTitle, $graduationDate);

    if(!checkResult){
        return;
    }

    let education;

    let idEducation = $inputEducationId.val();
    let peopleId = $inputPeopleId.val();

    $educationCollection = $('#educationCollection-' + peopleId);

    // nova
    if(idEducation == NEW_EDUCATION){
        education = new Education(educationIdCounter,$universityNameText, $universityNameSelect.val(), $studiesTypeText, $studiesTypeSelect.val(), $universityTitle, $graduationDate, peopleId);
        educationIdCounter++;
        educations.push(education);

        peopleEducations[peopleId].push(education);

        $educationCollection.append(educationListElem(education));

        // zbog edit i remove
        $('.tooltipped').tooltip();
    }
    else{
        education = findEducation(idEducation);

        education.changeData($universityNameText, $universityNameSelect.val(), $studiesTypeText, $studiesTypeSelect.val(), $universityTitle, $graduationDate);

        $educationData = $('#education-data-' + education.id);
        $educationData.empty();
        $educationData.append(educationDataContent(education));
    }

    $modalEducation.modal('close');
}

function checkJobInput(institutionName, jobPosition, jobStartDate, jobEndDate){
    let result = true;

    $('.job-error').hide();

    if(institutionName == ''){
        $('#institutionNameSelect-required').show();
        result = false;
    }

    if(jobPosition == ''){
        $('#jobPosition-required').show();
        result = false;
    }

    if(jobPosition.length > 255){
        $('#jobPosition-maxlen').show();
        result = false;
    }

    if(jobStartDate == ''){
        $('#jobStartDate-required').show();
        result = false;
    } else {
        let daysPassed = moment([]).diff(convertToDateObject(jobStartDate).toString(), "days");

        if (daysPassed < 0) {
            $('#jobStartDate-future').show();
            result = false;
        }
    }

    if(jobEndDate != ''){
        let daysPassed = moment([]).diff(convertToDateObject(jobEndDate).toString(), "days");

        if (daysPassed < 0) {
            $('#jobEndDate-future').show();
            result = false;
        }

        if(moment(convertToDateObject(jobEndDate)).diff(convertToDateObject(jobStartDate).toString(), "days") < 0 ){
            $('#jobStartDate-after').show();
            $('#jobEndDate-before').show();
            result = false;
        }
    }

    return result;
}

function saveJob(){
    $institutionNameSelect = $modalJob.find('#institutionNameSelect');
    $jobPosition = $modalJob.find('#jobPosition').val().trim();
    $jobStartDate = $modalJob.find('#jobStartDate').val();
    $jobEndDate = $modalJob.find('#jobEndDate').val();

    $inputPeopleId = $modalJob.find('#peopleId');
    $inputJobId = $modalJob.find("#jobId");

    $institutionNameText = $institutionNameSelect.children("option:selected").text();

    let checkResult = checkJobInput($institutionNameText, $jobPosition, $jobStartDate, $jobEndDate);

    if(!checkResult){
        return;
    }

    let job;

    let idJob = $inputJobId.val();
    let peopleId = $inputPeopleId.val();

    $jobCollection = $('#jobCollection-' + peopleId);

    // nova
    if(idJob == NEW_JOB){
        job = new Job(jobIdCounter,$institutionNameText, $institutionNameSelect.val(), $jobPosition, $jobStartDate, $jobEndDate, peopleId);
        jobIdCounter++;
        jobs.push(job);

        peopleJobs[peopleId].push(job);

        $jobCollection.append(jobListElem(job));

        // zbog edit i remove
        $('.tooltipped').tooltip();
    }
    else{
        job = findJob(idJob);

        job.changeData($institutionNameText, $institutionNameSelect.val(), $jobPosition, $jobStartDate, $jobEndDate);

        $jobData = $('#job-data-' + job.id);
        $jobData.empty();
        $jobData.append(jobDataContent(job));
    }

    $modalJob.modal('close');
}

function checkReferenceInput(referenceWorkCategory, referenceWorkTitle, referenceWorkIdentifier, referenceWorkMagazines, referenceWorkSigners){
    let result = true;

    $('.reference-error').hide();

    if(referenceWorkCategory == ''){
        $('#referenceWorkCategorySelect-required').show();
        result = false;
    }

    if(referenceWorkTitle == ''){
        $('#referenceWorkTitle-required').show();
        result = false;
    }

    if(referenceWorkTitle.length > 255){
        $('#referenceWorkTitle-maxlen').show();
        result = false;
    }

    if(referenceWorkIdentifier == ''){
        $('#referenceWorkIdentifier-required').show();
        result = false;
    }

    if(referenceWorkIdentifier.length > 255){
        $('#referenceWorkIdentifier-maxlen').show();
        result = false;
    }

    if(referenceWorkMagazines == ''){
        $('#referenceWorkMagazines-required').show();
        result = false;
    }

    if(referenceWorkMagazines.length > 1000){
        $('#referenceWorkMagazines-maxlen').show();
        result = false;
    }

    if(referenceWorkSigners == ''){
        $('#referenceWorkSigners-required').show();
        result = false;
    }

    if(referenceWorkSigners.length > 255){
        $('#referenceWorkSigners-maxlen').show();
        result = false;
    }

    return result;
}


function saveReference(){
    $referenceWorkCategorySelect = $modalReference.find('#referenceWorkCategorySelect');
    $referenceWorkTitle = $modalReference.find('#referenceWorkTitle').val().trim();
    $referenceWorkIdentifier = $modalReference.find('#referenceWorkIdentifier').val().trim();
    $referenceWorkMagazines = $modalReference.find('#referenceWorkMagazines').val().trim();
    $referenceWorkSigners = $modalReference.find('#referenceWorkSigners').val().trim();

    $inputPeopleId = $modalReference.find('#peopleId');
    $inputReferenceId = $modalReference.find("#referenceId");

    $referenceWorkCategoryText = $referenceWorkCategorySelect.children("option:selected").text();

    let checkResult = checkReferenceInput($referenceWorkCategoryText, $referenceWorkTitle, $referenceWorkIdentifier, $referenceWorkMagazines, $referenceWorkSigners );

    if(!checkResult){
        return;
    }

    let reference;

    let idReference = $inputReferenceId.val();
    let peopleId = $inputPeopleId.val();

    $referenceCollection = $('#referenceCollection-' + peopleId);

    // nova
    if(idReference == NEW_REFERENCE){
        reference = new Reference(referenceIdCounter, $referenceWorkCategoryText, $referenceWorkCategorySelect.val(), $referenceWorkTitle, $referenceWorkIdentifier, $referenceWorkMagazines, $referenceWorkSigners, peopleId);
        referenceIdCounter++;
        references.push(reference);

        if(references.length == MAX_REFERENCES){
            $('#' + reference.peopleId + '-reference-add-button').addClass('disabled');
        }

        peopleReferences[peopleId].push(reference);

        $referenceCollection.append(referenceListElem(reference));

        // zbog edit i remove
        $('.tooltipped').tooltip();
    }
    else{
        reference = findReference(idReference);

        reference.changeData($referenceWorkCategoryText, $referenceWorkCategorySelect.val(), $referenceWorkTitle, $referenceWorkIdentifier, $referenceWorkMagazines, $referenceWorkSigners);

        $referenceData = $('#reference-data-' + reference.id);
        $referenceData.empty();
        $referenceData.append(referenceDataContent(reference));
    }

    $modalReference.modal('close');
}

function deleteEducationDialog(idEducation){
    $inputEducationId = $modalDeleteEducation.find("#deleteEducationId");

    $inputEducationId.val(idEducation);

    $modalDeleteEducation.modal('open');
}

function deleteJobDialog(idJob){
    $inputJobId = $modalDeleteJob.find("#deleteJobId");

    $inputJobId.val(idJob);

    $modalDeleteJob.modal('open');
}

function deleteReferenceDialog(idReference){
    $inputReferenceId = $modalDeleteReference.find("#deleteReferenceId");

    $inputReferenceId.val(idReference);

    $modalDeleteReference.modal('open');
}


function deleteEducation(){
    $inputEducationId = $modalDeleteEducation.find("#deleteEducationId");

    let idEducation =  $inputEducationId.val();

    $('#education-elem-'+ idEducation).remove();

    let education = removeEducation(idEducation);

}

function deleteJob(){
    $inputJobId = $modalDeleteJob.find("#deleteJobId");

    let idJob =  $inputJobId.val();

    $('#job-elem-'+ idJob).remove();

    let job = removeJob(idJob);


}
function deleteReference(){
    $inputReferenceId = $modalDeleteReference.find("#deleteReferenceId");

    let idReference =  $inputReferenceId.val();

    $('#reference-elem-'+ idReference).remove();

    let reference = removeReference(idReference);

    $('#' + reference.peopleId + '-reference-add-button').removeClass('disabled');
}

function refreshUniversitySelects(){
    $universityNameSelect.formSelect();
    $studiesTypeSelect.formSelect();
}

function refreshJobSelects(){
    $institutionNameSelect.formSelect();
}

function refreshReferenceSelects(){
    $referenceWorkCategorySelect.formSelect();
}

function editEducation(idEducation){
    $universityNameSelect = $modalEducation.find('#universityNameSelect');
    $studiesTypeSelect = $modalEducation.find('#studiesTypeSelect');
    $universityTitle = $modalEducation.find('#universityTitle');
    $graduationDate = $modalEducation.find('#graduationDate');

    $inputPeopleId = $modalEducation.find("#peopleId");
    $inputEducationId = $modalEducation.find("#educationId");

    let education = findEducation(idEducation);

    $universityNameSelect.val(education.universityId);
    $studiesTypeSelect.val(education.studiesTypeId);

    refreshUniversitySelects();

    $universityTitle.val(education.universityTitle);
    $graduationDate.val(education.graduationDate);
    $inputPeopleId.val(education.peopleId);
    $inputEducationId.val(education.id);

    $('.education-error').hide();
    $modalEducation.modal('open');

    // DA NE BI DOSLO DO PREKLAPANJA LABELE I TEKSTA KAD SE EDITUJE PROJEKAT
    $modalEducation.find('.label-to-activate').addClass('active');
}

function editJob(idJob){
    $institutionNameSelect = $modalJob.find('#institutionNameSelect');
    $jobPosition = $modalJob.find('#jobPosition');
    $jobStartDate = $modalJob.find('#jobStartDate');
    $jobEndDate = $modalJob.find('#jobEndDate');

    $inputPeopleId = $modalJob.find("#peopleId");
    $inputJobId = $modalJob.find("#jobId");

    let job = findJob(idJob);

    $institutionNameSelect.val(job.institutionId);

    refreshJobSelects();

    $jobPosition.val(job.jobPosition);
    $jobStartDate.val(job.jobStartDate);
    $jobEndDate.val(job.jobEndDate);

    $inputPeopleId.val(job.peopleId);
    $inputJobId.val(job.id);

    $('.job-error').hide();
    $modalJob.modal('open');

    // DA NE BI DOSLO DO PREKLAPANJA LABELE I TEKSTA KAD SE EDITUJE PROJEKAT
    $modalJob.find('.label-to-activate').addClass('active');
}

function editReference(idReference){
    $referenceWorkCategorySelect = $modalReference.find('#referenceWorkCategorySelect');
    $referenceWorkTitle = $modalReference.find('#referenceWorkTitle');
    $referenceWorkIdentifier = $modalReference.find('#referenceWorkIdentifier');
    $referenceWorkMagazines = $modalReference.find('#referenceWorkMagazines');
    $referenceWorkSigners = $modalReference.find('#referenceWorkSigners');

    $inputPeopleId = $modalReference.find('#peopleId');
    $inputReferenceId = $modalReference.find("#referenceId");

    let reference = findReference(idReference);

    $referenceWorkCategorySelect.val(reference.referenceWorkCategoryId);

    refreshReferenceSelects();

    $referenceWorkTitle.val(reference.referenceWorkTitle);
    $referenceWorkIdentifier.val(reference.referenceWorkIdentifier);
    $referenceWorkMagazines.val(reference.referenceWorkMagazines);
    $referenceWorkSigners.val(reference.referenceWorkSigners);

    $inputPeopleId.val(reference.peopleId);
    $inputReferenceId.val(reference.id);

    $('.reference-error').hide();
    $modalReference.modal('open');

    // DA NE BI DOSLO DO PREKLAPANJA LABELE I TEKSTA KAD SE EDITUJE PROJEKAT
    $modalReference.find('.label-to-activate').addClass('active');
}

function newEducation(peopleId){
    $universityNameSelect = $modalEducation.find('#universityNameSelect');
    $studiesTypeSelect = $modalEducation.find('#studiesTypeSelect');
    $universityTitle = $modalEducation.find('#universityTitle');
    $graduationDate = $modalEducation.find('#graduationDate');
    $inputPeopleId = $modalEducation.find('#peopleId');
    $inputEducationId = $modalEducation.find("#educationId");

    $universityNameSelect.val($("#universityNameSelect option:first").val());
    $studiesTypeSelect.val($("#studiesTypeSelect option:first").val());

    refreshUniversitySelects();

    $universityTitle.val("");
    $graduationDate.val("");
    $inputPeopleId.val(peopleId);
    $inputEducationId.val(-1);

    $('.education-error').hide();
    $modalEducation.modal('open');
}

function newJob(peopleId){
    $institutionNameSelect = $modalJob.find('#institutionNameSelect');
    $jobPosition = $modalJob.find('#jobPosition');
    $jobStartDate = $modalJob.find('#jobStartDate');
    $jobEndDate = $modalJob.find('#jobEndDate');

    $inputPeopleId = $modalJob.find("#peopleId");
    $inputJobId = $modalJob.find("#jobId");;

    $institutionNameSelect.val($("#institutionNameSelect option:first").val());

    refreshJobSelects();

    $jobPosition.val("");
    $jobStartDate.val("");
    $jobEndDate.val("");

    $inputPeopleId.val(peopleId);
    $inputJobId.val(-1);

    $('.job-error').hide();
    $modalJob.modal('open');
}

function newReference(peopleId){
    $referenceWorkCategorySelect = $modalReference.find('#referenceWorkCategorySelect');
    $referenceWorkTitle = $modalReference.find('#referenceWorkTitle');
    $referenceWorkIdentifier = $modalReference.find('#referenceWorkIdentifier');
    $referenceWorkMagazines = $modalReference.find('#referenceWorkMagazines');
    $referenceWorkSigners = $modalReference.find('#referenceWorkSigners');

    $inputPeopleId = $modalReference.find('#peopleId');
    $inputReferenceId = $modalReference.find("#referenceId");

    $referenceWorkCategorySelect.val($("#referenceWorkCategorySelect option:first").val());

    refreshReferenceSelects();

    $referenceWorkTitle.val("");
    $referenceWorkIdentifier.val("");
    $referenceWorkMagazines.val("");
    $referenceWorkSigners.val("");

    $inputPeopleId.val(peopleId);
    $inputReferenceId.val(-1);

    $('.reference-error').hide();
    $modalReference.modal('open');
}

//TODO
function memberCheckBoxChanged(peopleId){
    memberFlags[peopleId] = $('#member-' + peopleId + '-CB').is(":checked");

    $memberContent = $('#member-' + peopleId + '-content');

    if(memberFlags[peopleId]){
        $memberContent.show();
        teamNum++;
    }
    else{
        $memberContent.hide();
        teamNum--;
    }
}

function memberResearchAreaChanged(peopleId){
    $researchArea = $('#' + peopleId + "-researchArea");

    $researchAreaMaxLenError = $('#' + peopleId + '-researchArea-maxlen');

    $researchAreaMaxLenError.hide();

    if($researchArea.val().length > MAX_RESEARCH_AREA_SELECTS){
        $researchArea.val(membersResearchAreaLastValue[peopleId]);
        $researchArea.formSelect();
        $researchAreaMaxLenError.show();
    } else{
        membersResearchAreaLastValue[peopleId] = $researchArea.val();
    }


}

function biographyListElem(peopleId, firstName, lastName){
    return '<a href="#!" class="collection-item">\n' +
                '<h6 style="color:black;">Учесник ' + peopleId + ' - '  + firstName + ' ' + lastName +'</h6>' +
                '<div class="file-field input-field">\n' +
'                    <div class="btn">\n' +
'                      <span>Библиографијa учесника <span class="asterisk">*</span></span>\n' +
'                      <input onchange="uploadPeopleBibliographyFile(this, ' + peopleId + ', \'bibliografija\', [\'pdf\']);" accept="application/pdf" type="file">\n' +
'                    </div>\n' +
'                    <div class="file-path-wrapper">\n' +
'                      <input id="bibliography-' + peopleId + '-filepath" class="file-path validate" type="text">\n' +
'                      <span id="bibliography-' + peopleId +  '-extension" class="helper-text file-error" style="display: none;">Приложени фајл мора бити у PDF формату.</span>\n' +
'                      <span id="bibliography-' + peopleId +  '-size" class="helper-text file-error" style="display: none;">Приложени фајл мора бити мањи од 5MB.</span>\n' +
'                      <span id="bibliography-' + peopleId +  '-success" class="helper-text file-success" style="display: none;">Приложени фајл је успешно пребачен на сервер.</span>\n' +
'                    </div>\n' +
'                 </div>\n' +
'                 <div class="file-field input-field">\n' +
'                    <div class="btn">\n' +
'                        <span>Биографијa учесника <span class="asterisk">*</span></span>\n' +
'                        <input onchange="uploadPeopleBiographyFile(this, ' + peopleId + ', \'biografija\', [\'pdf\']);" accept="application/pdf" type="file">\n' +
'                    </div>\n' +
'                 <div class="file-path-wrapper">\n' +
'                    <input id="biography-' + peopleId + '-filepath"  class="file-path validate" type="text">\n' +
'                      <span id="biography-' + peopleId +  '-extension" class="helper-text file-error" style="display: none;">Приложени фајл мора бити у PDF формату.</span>\n' +
'                      <span id="biography-' + peopleId +  '-size" class="helper-text file-error" style="display: none;">Приложени фајл мора бити мањи од 5MB.</span>\n' +
'                      <span id="biography-' + peopleId +  '-success" class="helper-text file-success" style="display: none;">Приложени фајл је успешно пребачен на сервер.</span>\n' +
'                 </div>\n' +
'                    </div>' +
           '</a>';
}

function renderBiographyListElem(peopleId){
    $biographyCollection = $('#biography-collection');

    $firstName = $('#' + peopleId + '-firstName').val();
    $lastName = $('#' + peopleId + '-lastName').val();

    $biographyCollection.append(biographyListElem(peopleId, $firstName, $lastName));
}

//TODO
function nextPage(){
    page++;

    window.scrollTo(0, 0);

    $('#page-' + (page - 1)).hide();

    $('#page-' + page).show();

    $biographyCollection = $('#biography-collection');

    $biographyCollection.empty();

    if(teamNum > 1) {
        $biographyCollection.show();

        for (let i = 1; i <= MAX_TEAM; i++) {
            if (memberFlags[i]) {
                renderBiographyListElem(i);

                // ako su ranije bili dodati fajlovi za te ucesnike, zadrzi ih
                if(bibliographyFlags[i]){
                    $('#bibliography-' + i + '-filepath').val(bibliographyFileNames[i]);
                }

                if(biographyFlags[i]){
                    $('#biography-' + i + '-filepath').val(biographyFileNames[i]);
                }
            }
        }

        if(submitted){
            $('input').attr("disabled", true);
        }
    }
    else{
        $biographyCollection.hide();
    }

    $('.file-error').hide();
    $('.file-success').hide();
}

//TODO
function prevPage(){
    page--;

    window.scrollTo(0, 0);

    $('#page-' + (page + 1)).hide();

    $('#page-' + page).show();
}

function loadProject(){
    $projectTitle = $('#projectTitle').val().trim();
    $abstract = $('#abstractTA').val().trim();
    $id_area_project = $('#areaSelect').val();

    if($id_area_project == ''){
        $id_area_project = null;
    }

    $budget = $('#budgetText').val().trim();
    $budget = $budget.replace(/,/g, "");

    if($budget) {
        $budget = parseInt($budget);
    } else{
        $budget = null;
    }

    return new Project($projectTitle, $abstract, $id_area_project, $budget);
}

function checkProjectDraft(project, errorMessages){
    if(project.title == ''){
        errorMessages.push('Како би се пријава снимила, мора се унети бар наслов пројекта.');
    }

    if(project.title.length > 255){
        errorMessages.push('Наслов пројекта не сме садржати више од 255 карактера.');
    }

    if(project.abstract.length > 2000){
        errorMessages.push('Апстракт пројекта не сме садржати више од 2000 карактера.');
    }

    if(project.budget) {
        if (isNaN(project.budget) || project.budget < 0 || project.budget > 200000) {
            errorMessages.push('Буџет пројекта мора бити број између 0 и 200000.');
        }
    }
}

function checkProjectSubmit(project, errorMessages){
    if(project.title == ''){
        errorMessages.push('Морате унети наслов пројекта.');
    }

    if(project.title.length > 255){
        errorMessages.push('Наслов пројекта не сме садржати више од 255 карактера.');
    }

    if(project.abstract == ''){
        errorMessages.push('Морате унети апстракт пројекта.');
    }

    if(project.abstract.length > 2000){
        errorMessages.push('Апстракт пројекта не сме садржати више од 2000 карактера.');
    }

    if(project.budget) {
        if (isNaN(project.budget) || project.budget < 0 || project.budget > 200000) {
            errorMessages.push('Буџет пројекта мора бити број између 0 и 200000.');
        }
    } else{
        errorMessages.push('Морате унети буџет пројекта.');
    }

    if(project.id_area_project == null){
        errorMessages.push('Морате унети област пројекта.');
    }
}

function loadMember(peopleId){
    $firstName = $('#' + peopleId + '-firstName').val().trim();
    $lastName = $('#' + peopleId + '-lastName').val().trim();
    $birthDate = $('#' + peopleId + '-birth').val();
    $id_area_research = $('#' + peopleId + '-researchArea').val();

    if($id_area_research == ''){
        $id_area_research = null;
    }

    if($birthDate == ''){
        $birthDate = null;
    } else{
        $birthDate = dateSqlFormat($birthDate);
    }

    let person = new Person($firstName, $lastName, $birthDate, $id_area_research);

    for(let i = 0; i < peopleEducations[peopleId].length; i++){
        let education = peopleEducations[peopleId][i];
        $id_education_institution = education.universityId;
        $id_studies_degree = education.studiesTypeId;
        $title = education.universityTitle;

        if(education.graduationDate != ''){
            $date = dateSqlFormat( education.graduationDate); // pogodan format za bazu
        } else{
            $date = '';
        }


        let serverEducation = new ServerEducation($id_education_institution, $id_studies_degree, $title, $date);

        person.addEducation(serverEducation);
    }

    for(let i = 0; i < peopleJobs[peopleId].length; i++){
        let job = peopleJobs[peopleId][i];

        if(job.jobStartDate != ''){
            $date_start = dateSqlFormat(job.jobStartDate); // pogodan format za bazu
        } else{
            $date_start = '';
        }


        if(job.jobEndDate != ''){
            $date_end = dateSqlFormat(job.jobEndDate); // pogodan format za bazu
        } else{
            $date_end =  '';
        }

        $id_scientific_institution = job.institutionId;
        $position = job.jobPosition;

        if($date_end == ''){
            $date_end = null;
        }

        let serverJob = new ServerJob($date_start, $date_end, $id_scientific_institution, $position);

        person.addJob(serverJob);
    }

    for(let i = 0; i < peopleReferences[peopleId].length; i++){
        let reference = peopleReferences[peopleId][i];

        $identifier = reference.referenceWorkIdentifier;
        $title = reference.referenceWorkTitle;
        $id_category_reference = reference.referenceWorkCategoryId;
        $announcement_places = reference.referenceWorkMagazines;
        $signers = reference.referenceWorkSigners;

        let serverReference = new ServerReference($identifier, $title, $id_category_reference, $announcement_places, $signers);

        person.addReference(serverReference);
    }

    return person;
}

function checkPersonDraft(person, peopleId, errorMessages) {
    let errorStart = peopleId == 0 ? 'Руководилац пројекта - ' : 'Учесник ' + peopleId + ' - ';

    if (person.firstName.length > 64) {
        errorMessages.push(errorStart + 'Име не сме садржати више од 63 карактера.');
    }

    if (person.lastName.length > 64) {
        errorMessages.push(errorStart + 'Презиме не сме садржати више од 63 карактера.');
    }

    if (person.birthDate != null) {
        let dateObject = convertToDateObject(person.birthDate);

        let yearsPassed = moment([]).diff(dateObject.toString(), "years");

        if (yearsPassed < 18) {
            errorMessages.push(errorStart + 'Mора бити пунолетан.');
        }
    }
}

function checkPersonSubmit(person, peopleId, errorMessages) {
    let errorStart = peopleId == 0 ? 'Руководилац пројекта - ' : 'Учесник ' + peopleId + ' - ';

    if (person.firstName == '') {
        errorMessages.push(errorStart + 'Морате унети име.');
    }

    if (person.firstName.length > 64) {
        errorMessages.push(errorStart + 'Име не сме садржати више од 63 карактера.');
    }

    if (person.lastName == '') {
        errorMessages.push(errorStart + 'Морате унети презиме.');
    }

    if (person.lastName.length > 64) {
        errorMessages.push(errorStart + 'Презиме не сме садржати више од 63 карактера.');
    }

    if (person.birthDate != null) {
        let dateObject = convertToDateObject(person.birthDate);

        let yearsPassed = moment([]).diff(dateObject.toString(), "years");

        if (yearsPassed < 18) {
            errorMessages.push(errorStart + 'Mора бити пунолетан.');
        }
    } else{
        errorMessages.push(errorStart + 'Морате унети датум рођења.');
    }

    if(person.id_area_research == null){
        errorMessages.push(errorStart + 'Морате унети бар једну област истраживања.');
    }

    if(peopleEducations[peopleId].length == 0){
        errorMessages.push(errorStart + 'Морате унети образовање.');
    }
}

function draftErrorsElem(errorMessages, i){
    return '<li class="collection-item" style="color:red">' + errorMessages[i] + '</li>';
}

function draftErrorsHeader(){
    return '<li class="collection-header"><h6>Грешке при снимању</h6></li>';
}

function saveDraft(id){
    let errorMessages = [];

    let project = loadProject();

    checkProjectDraft(project, errorMessages);

    for(let i = 0; i < memberFlags.length; i++){
        if(memberFlags[i]) {
            let person = loadMember(i);

            checkPersonDraft(person,i, errorMessages);

            project.addPerson(person);
        }
    }

    let filesDescriptor = new FilesDescriptor(projectFilesFlags, statementFilesCount, bibliographyFlags, biographyFlags);

    project.setFilesDescriptor(filesDescriptor);

    $draftErrorsCollection = $('#draftErrors-collection');

    if(errorMessages.length == 0) {
        $draftErrorsCollection.hide();
        $modalLoader.modal('open');
        $.ajax({
            url: '/snimi-projekat/' + id + '/0',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(project),
            dataType: 'json',
            success: function (result) {
                $modalLoader.modal('close');
                $closeSaveSuccessfulButton =  $modalSaveSuccessful.find('#close-save-successful-dialog');
                $closeSaveSuccessfulButton.attr('href', '/predlog-projekta/' + result);
                $modalSaveSuccessful.modal('open');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $modalLoader.modal('close');
                $modalSaveFailed.modal('open');
            }
        });
    } else{
        $draftErrorsCollection.empty();

        $draftErrorsCollection.append(draftErrorsHeader());

        for(let i = 0; i < errorMessages.length; i++){
            $draftErrorsCollection.append(draftErrorsElem(errorMessages, i));
        }

        $('#draftErrors-collection').show();
    }

}

function uploadProjectFile(fileInput, fileOrdinaryNum, fileDirToSave, supportedExtensions){
    let fileName;

    if(fileInput.files.length > 0){
        fileName = fileInput.files[0].name;
    }  else{
        fileName = '';
    }

    $fileErrorMessage = $('#file-' + fileOrdinaryNum + '-extension');
    $fileSizeErrorMessage = $('#file-' + fileOrdinaryNum + '-size');
    $fileSuccessMessage = $('#file-' + fileOrdinaryNum + '-success');

    $fileErrorMessage.hide();
    $fileSizeErrorMessage.hide();
    $fileSuccessMessage.hide();

    let extensionRegex = /\.([^\.]+)$/;

    projectFilesFlags[fileOrdinaryNum] = false;

    if(fileName == '' || fileName.match(extensionRegex) == null){
        fileInput.value = '';
        $fileErrorMessage.show();
        return;
    }

    let extension = fileName.match(extensionRegex)[1].toLowerCase();

    if(supportedExtensions.includes(extension)){
        if(fileInput.files[0].size < 10485760 ){ // 10 MB
            let formdata = new FormData();
            formdata.append("file", fileInput.files[0]);
            $.ajax({
                url: '/uploadProjectFile/' + fileDirToSave,
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (result) {
                    projectFilesFlags[fileOrdinaryNum] = true;
                    $fileSuccessMessage.show();
                    fileInput.value = '';  // da se ne bi onchange triggerovao na cancel
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    fileInput.value = '';
                    $fileErrorMessage.show();
                    $fileSizeErrorMessage.show();
                }
            });
        } else{
            fileInput.value = '';
            $fileSizeErrorMessage.show();
        }
    } else{
        fileInput.value = '';
        $fileErrorMessage.show();
    }
}

function uploadMultipleStatementFiles(fileInput, fileOrdinaryNum, supportedExtensions) {
    $fileErrorMessage = $('#file-' + fileOrdinaryNum + '-extension');
    $fileSizeErrorMessage = $('#file-' + fileOrdinaryNum + '-size');
    $fileCountErrorMessage = $('#file-' + fileOrdinaryNum + '-count');
    $fileSuccessMessage = $('#file-' + fileOrdinaryNum + '-success');

    $fileErrorMessage.hide();
    $fileSizeErrorMessage.hide();
    $fileCountErrorMessage.hide();
    $fileSuccessMessage.hide();

    let extensionRegex = /\.([^\.]+)$/;

    projectFilesFlags[fileOrdinaryNum] = false;

    statementFilesCount = 0;


    let extensionsValid = true;
    let fileSizeValid = true;
    let fileCountValid = true;

    if(fileInput.files.length > MAX_STATEMENT_FILES){
        fileCountValid = false;
    }
    else if (fileInput.files.length > 0){
        for (let i = 0; i < fileInput.files.length; i++) {
            let fileName = fileInput.files[i].name;

            if (fileName == '' || fileName.match(extensionRegex) == null) {
                extensionsValid = false;
                break;
            }

            if (!supportedExtensions.includes(fileName.match(extensionRegex)[1].toLowerCase())) {
                extensionsValid = false;
                break;
            }

            // 5 MB
            if(fileInput.files[i].size >= 5242880  ) {
                fileSizeValid = false;
                break;
            }
        }
    } else{
        extensionsValid = false;
    }

    if(extensionsValid || fileSizeValid || fileCountValid) {
        let formdata = new FormData();

        for(let i = 0; i < fileInput.files.length; i++) {
            formdata.append("files[]", fileInput.files[i]);
        }

        $.ajax({
            url: '/uploadStatementFiles',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (result) {
                projectFilesFlags[fileOrdinaryNum] = true;
                $fileSuccessMessage.show();
                statementFilesCount = fileInput.files.length;
                fileInput.value = '';  // da se ne bi onchange triggerovao na cancel
            },
            error: function (xhr, ajaxOptions, thrownError) {
                fileInput.value = '';
                $fileErrorMessage.show();
                $fileCountErrorMessage.show();
                $fileSizeErrorMessage.show();
            }
        });
    }else{
        if(!extensionsValid) {
            $fileErrorMessage.show();
        } else if(!fileSizeValid){
            $fileSizeErrorMessage.show();
        } else if(!fileCountValid){
            $fileCountErrorMessage.show();
        }
    }
}

//stringPrefix je ili bibliography ili biography
function uploadPersonFile(stringPrefix, peopleId, fileInput, supportedExtensions, flagArray, fileNameArray, fileDirToSave){
    let fileName;

    if(fileInput.files.length > 0){
        fileName = fileInput.files[0].name;
    }  else{
        fileName = '';
    }

    $fileErrorMessage = $('#'+ stringPrefix + '-' + peopleId + '-extension');
    $fileSizeErrorMessage = $('#'+ stringPrefix + '-' + peopleId + '-size');
    $fileSuccessMessage = $('#'+ stringPrefix + '-' + peopleId + '-success');

    $fileErrorMessage.hide();
    $fileSizeErrorMessage.hide();
    $fileSuccessMessage.hide();

    let extensionRegex = /\.([^\.]+)$/;

    flagArray[peopleId] = false;
    fileNameArray[peopleId] = '';

    if(fileName == '' || fileName.match(extensionRegex) == null){
        fileInput.value = '';
        $fileErrorMessage.show();
        return;
    }

    let extension = fileName.match(extensionRegex)[1].toLowerCase();

    if(supportedExtensions.includes(extension)){
        // 5 MB
        if(fileInput.files[0].size < 5242880  ) {
            let formdata = new FormData();
            formdata.append("file", fileInput.files[0]);

            $.ajax({
                url: '/uploadPersonFile/' + peopleId + '/' + fileDirToSave,
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (result) {
                    flagArray[peopleId] = true;
                    fileNameArray[peopleId] = fileName;
                    $fileSuccessMessage.show();
                    fileInput.value = ''; // da se ne bi onchange triggerovao na cancel
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    fileInput.value = '';
                    $fileErrorMessage.show();
                    $fileSizeErrorMessage.show();
                }
            });
        } else{
            fileInput.value = '';
            $fileSizeErrorMessage.show();
        }
    } else{
        fileInput.value = '';
        $fileErrorMessage.show();
    }
}

function uploadPeopleBibliographyFile(fileInput, peopleId, fileDirToSave, supportedExtensions){
    uploadPersonFile('bibliography', peopleId, fileInput, supportedExtensions, bibliographyFlags, bibliographyFileNames,  fileDirToSave);
}

function uploadPeopleBiographyFile(fileInput, peopleId, fileDirToSave, supportedExtensions){
    uploadPersonFile('biography', peopleId, fileInput, supportedExtensions, biographyFlags, biographyFileNames, fileDirToSave);
}

function checkFilesDescriptorSubmit(filesDescriptor, errorMessages){
    let errorHappened = false;

    for(let i = 0; i < filesDescriptor.projectFilesFlags.length; i++){
        if(!filesDescriptor.projectFilesFlags[i]){
            errorHappened = true;
            break;
        }
    }

    if(!errorHappened) {
        if (filesDescriptor.statementFilesCount == 0) {
            errorHappened = true;
        }
    }

    if(!errorHappened) {
        for(let i = 0; i < filesDescriptor.bibliographyFlags.length; i++){
            if(!filesDescriptor.bibliographyFlags[i]){
                errorHappened = true;
                break;
            }
        }
    }

    if(!errorHappened) {
        for (let i = 0; i < filesDescriptor.biographyFlags.length; i++) {
            if (!filesDescriptor.biographyFlags[i]) {
                errorHappened = true;
                break;
            }
        }
    }

    if(errorHappened) {
        errorMessages.push('Морате изабрати све пропратне фајлове');
    }

}

function submitProject(id){
    let errorMessages = [];

    let project = loadProject();

    if(!$('#conditions-CB').is(':checked')){
        errorMessages.push('Морате прихватити услове коришћења.');
    } else{
        project.agreeConditions();
    }

    checkProjectSubmit(project, errorMessages);

    for(let i = 0; i < memberFlags.length; i++){
        if(memberFlags[i]) {
            let person = loadMember(i);

            checkPersonSubmit(person,i, errorMessages);

            project.addPerson(person);
        }
    }

    let filesDescriptor = new FilesDescriptor(projectFilesFlags, statementFilesCount, bibliographyFlags, biographyFlags);

    checkFilesDescriptorSubmit(filesDescriptor, errorMessages);

    project.setFilesDescriptor(filesDescriptor);

    $draftErrorsCollection = $('#draftErrors-collection');

    if(errorMessages.length == 0) {
        $draftErrorsCollection.hide();
        $modalLoader.modal('open');
        $.ajax({
            url: '/snimi-projekat/' + id + '/1',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(project),
            dataType: 'json',
            success: function (result) {
                $modalLoader.modal('close');
                $closeSaveSuccessfulButton =  $modalSaveSuccessful.find('#close-save-successful-dialog');
                $closeSaveSuccessfulButton.attr('href', '/predlog-projekta/' + result);
                $modalSaveSuccessful.modal('open');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $modalLoader.modal('close');
                $modalSaveFailed.modal('open');
            }
        });
    } else{
        $draftErrorsCollection.empty();

        $draftErrorsCollection.append(draftErrorsHeader());

        for(let i = 0; i < errorMessages.length; i++){
            $draftErrorsCollection.append(draftErrorsElem(errorMessages, i));
        }

        $('#draftErrors-collection').show();
    }
}