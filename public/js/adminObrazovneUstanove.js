function showAvailable(){
    $('#tabela-obrazovne_ustanove-obrisani').css('display', 'none');
    $('#tabela-obrazovne_ustanove-aktivni').css('display', 'block');
    $('#tabela-obrazovne_ustanove-obrisani_wrapper').css('display', 'none');
    $('#tabela-obrazovne_ustanove-aktivni_wrapper').css('display', 'block');

    $('#obrazovne_ustanove-title').html('Доступне образовне установе');
}

function showUnavailable(){
    $('#tabela-obrazovne_ustanove-obrisani').css('display', 'block');
    $('#tabela-obrazovne_ustanove-aktivni').css('display', 'none');
    $('#tabela-obrazovne_ustanove-obrisani_wrapper').css('display', 'block');
    $('#tabela-obrazovne_ustanove-aktivni_wrapper').css('display', 'none');

    $('#obrazovne_ustanove-title').html('Обрисане образовне установе');
}