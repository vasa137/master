<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::middleware(['auth'])->group(function () {

    Route::get('/home', function () {
        if(Auth::user()->admin){
            return redirect('/admin');
        } else {
            return redirect('/moji-projekti');
        }
    });

    Route::get('/moji-projekti', 'ProjectProposalController@myProjects');

    Route::get('/predlog-projekta/{id}', 'ProjectProposalController@projectApplication');

    Route::post('/uploadProjectFile/{fileDirToSave}', 'ProjectProposalController@uploadProjectFile');

    Route::post('/uploadStatementFiles', 'ProjectProposalController@uploadStatementFiles');

    Route::post('/uploadPersonFile/{personId}/{fileDirToSave}', 'ProjectProposalController@uploadPersonFile');

    Route::post('/snimi-projekat/{id}/{isFinal}', 'ProjectProposalController@saveProject');

    Route::get('/preuzimanje-datoteka/{id}', 'ProjectProposalController@downloadProjectArtifacts');

});

//---------ADMIN RUTE POCETAK----------------
Route::middleware(['admin'])->group(function () {
    Route::get('/admin', 'AdminController@home');

    Route::get('/admin/projekti', 'AdminController@projects');

    //--------------OBLASTI PROJEKATA---------------------
    Route::get('/admin/oblasti-projekata', 'AdminProjectAreaController@projectAreas');

    Route::get('/admin/oblast-projekat/{id}', 'AdminProjectAreaController@projectArea');

    Route::post('/admin/saveProjectArea/{id}', 'AdminProjectAreaController@saveProjectArea');

    Route::post('/admin/deleteProjectArea/{id}', 'AdminProjectAreaController@deleteProjectArea');

    Route::post('/admin/restaurateProjectArea/{id}', 'AdminProjectAreaController@restaurateProjectArea');

    //--------------OBLASTI ISTRAZIVANJA---------------------
    Route::get('/admin/oblasti-istrazivanja', 'AdminResearchAreaController@researchAreas');

    Route::get('/admin/oblast-istrazivanja/{id}', 'AdminResearchAreaController@researchArea');

    Route::post('/admin/saveResearchArea/{id}', 'AdminResearchAreaController@saveResearchArea');

    Route::post('/admin/deleteResearchArea/{id}', 'AdminResearchAreaController@deleteResearchArea');

    Route::post('/admin/restaurateResearchArea/{id}', 'AdminResearchAreaController@restaurateResearchArea');

    //--------------OBRAZOVNE USTANOVE---------------------
    Route::get('/admin/obrazovne-ustanove', 'AdminEducationInstitutionController@educationInstitutions');

    Route::get('/admin/obrazovna-ustanova/{id}', 'AdminEducationInstitutionController@educationInstitution');

    Route::post('/admin/saveEducationInstitution/{id}', 'AdminEducationInstitutionController@saveEducationInstitution');

    Route::post('/admin/deleteEducationInstitution/{id}', 'AdminEducationInstitutionController@deleteEducationInstitution');

    Route::post('/admin/restaurateEducationInstitution/{id}', 'AdminEducationInstitutionController@restaurateEducationInstitution');

    //--------------NAUCNE USTANOVE---------------------
    Route::get('/admin/naucne-ustanove', 'AdminScientificInstitutionController@scientificInstitutions');

    Route::get('/admin/naucna-ustanova/{id}', 'AdminScientificInstitutionController@scientificInstitution');

    Route::post('/admin/saveScientificInstitution/{id}', 'AdminScientificInstitutionController@saveScientificInstitution');

    Route::post('/admin/deleteScientificInstitution/{id}', 'AdminScientificInstitutionController@deleteScientificInstitution');

    Route::post('/admin/restaurateScientificInstitution/{id}', 'AdminScientificInstitutionController@restaurateScientificInstitution');

    //--------------KATEGORIJE RADOVA---------------------
    Route::get('/admin/kategorije-radova', 'AdminWorkCategoryController@workCategories');

    Route::get('/admin/kategorija-rada/{id}', 'AdminWorkCategoryController@workCategory');

    Route::post('/admin/saveWorkCategory/{id}', 'AdminWorkCategoryController@saveWorkCategory');

    Route::post('/admin/deleteWorkCategory/{id}', 'AdminWorkCategoryController@deleteWorkCategory');

    Route::post('/admin/restaurateWorkCategory/{id}', 'AdminWorkCategoryController@restaurateWorkCategory');
});


Auth::routes();
