@extends('admin.adminLayout')

@section('title')
    @if($izmena)
        Област истраживања - {{$oblast_istrazivanja->naziv}}
    @else
        Нова област истраживања
    @endif
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Админ</a>
    <a class="breadcrumb-item" href="/admin/oblasti-istrazivanja">Области истраживања</a>
    <span class="breadcrumb-item active">@if($izmena){{$oblast_istrazivanja->naziv}} @else Нова област истраживања @endif</span>
@stop

@section('heder-h1')
    @if($izmena){{$oblast_istrazivanja->naziv}} @else Нова област истраживања @endif
@stop



@section('main')
    <div class="row gutters-tiny">
    @if($izmena)

        <!-- In Orders -->
            <div class="col-md-3 col-xl-3">
                <a class="block block-rounded block-link-shadow" >
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-shopping-basket fa-2x text-info"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{$oblast_istrazivanja->broj_osoba}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Особа</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END In Orders -->
    @endif
    <!-- Stock -->
        <div class="col-md-3 col-xl-3">

            <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-oblast_istrazivanja-submit-button').click()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="si si-settings fa-2x text-success"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Сачувај</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Stock -->

    @if($izmena)
        @if(!$oblast_istrazivanja->sakriven)
            <!-- Delete Product -->
                <div class="col-md-3 col-xl-3">
                    <form id="forma-obrisi-oblast_istrazivanja" method="POST" action="/admin/deleteResearchArea/{{$oblast_istrazivanja->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-obrisi-oblast_istrazivanja').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-trash fa-2x text-danger"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-danger">
                                        <i class="fa fa-times"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Обриши</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>

            @else
                <div class="col-md-3 col-xl-3">
                    <form id="forma-restauriraj-oblast_istrazivanja" method="POST" action="/admin/restaurateResearchArea/{{$oblast_istrazivanja->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj-oblast_istrazivanja').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-lightbulb-o fa-2x text-warning"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-warning">
                                        <i class="fa fa-undo"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Рестаурирај</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>
        @endif
    @endif
    <!-- END Delete Product -->
    </div>
    <!-- END Overview -->
    <form id="forma-oblast_istrazivanja" method="POST" @if($izmena) action="/admin/saveResearchArea/{{$oblast_istrazivanja->id}}" @else action="/admin/saveResearchArea/-1" @endif>
    {{csrf_field()}}
    <!-- Update Product -->
        <h2 class="content-heading">Информације о области истраживања</h2>
        <div class="row gutters-tiny">
            <!-- Basic Info -->
            <div class="col-md-12">
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Информације</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="form-group row">
                            <label class="col-12" >Назив</label>
                            <div class="col-12 input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="si si-info"></i>
                                </span>
                                </div>
                                <input maxlength="254" type="text" class="form-control" name="naziv" @if($izmena) value="{{$oblast_istrazivanja->naziv}}" @endif required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12">Oпис</label>
                            <div class="col-12">
                                <!-- CKEditor (js-ckeditor id is initialized in Codebase() -> uiHelperCkeditor()) -->
                                <!-- For more info and examples you can check out http://ckeditor.com -->
                                <textarea maxlength="2000" class="form-control" name="opis" rows="8">@if($izmena){{$oblast_istrazivanja->opis}}@endif</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- END Basic Info -->



        </div>
        <!-- END More Options -->

        <!-- END Update Product -->
        <input type="submit" id="forma-oblast_istrazivanja-submit-button" style="display:none"/>
    </form>
@stop