@extends('admin.adminLayout')

@section('title')
    Пројекти
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/">Админ</a>
    <span class="breadcrumb-item active">Пројекти</span>
@stop

@section('heder-h1')
    Пројекти
@stop


@section('heder-h2')
    Пристигло је<a class="text-primary-light link-effect"> {{$brojProjekata}} пројеката</a>.
@stop

@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaProjekti.js')}}"></script>
@endsection

@section('main')
    <div class="row gutters-tiny">
        <!-- All Products -->
        <div class="col-md-6 col-xl-4">
            <a class="block block-rounded block-link-shadow">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-circle-o fa-2x text-info-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($projekti)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Укупно пројеката</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END All Products -->

        <!-- Top Sellers -->
        <div class="col-md-6 col-xl-4">
            <a class="block block-rounded block-link-shadow">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{$totalBudget}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Укупан буџет</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->

        <!-- Out of Stock -->
        <div class="col-md-6 col-xl-4">
            <a class="block block-rounded block-link-shadow">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-warning fa-2x text-danger-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{$totalMembers}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Укупно учесника</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Out of Stock -->
    </div>
    <!-- END Overview -->

    <!-- Dynamic Table Full Pagination -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Пројекти</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
            <table id="tabela-projekti" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center" style="width: 5%;">ИД</th>
                    <th class="text-center" style="width: 15%;">Време пријаве</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Наслов</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Област</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Руководилац</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Запослење руководиоца</th>
                    <th class="d-none d-sm-table-cell" style="width: 10%;">Буџет</th>
                    <th class="text-center" style="width: 5%;">Акција</th>
                </tr>
                </thead>
                <tbody>
                @foreach($projekti as $projekat)
                    <tr>
                        <td class="text-center">#{{$projekat->id}}</td>
                        <td class="text-center">{{date_format($projekat->updated_at, "Y-m-d H:i")}}</td>
                        <td class="font-w600">{{$projekat->naslov}}</td>
                        <td class="d-none d-sm-table-cell">{{$projekat->oblast_projekat->naziv}}</td>

                        <td class="d-none d-sm-table-cell">
                            {{$projekat->osobe[0]->ime}} {{$projekat->osobe[0]->prezime}}
                        </td>
                        <td class="d-none d-sm-table-cell">
                            @if($projekat->osobe[0]->trenutnaNaucnaUstanova != null)
                                {{$projekat->osobe[0]->trenutnaNaucnaUstanova->naziv}}
                            @endif

                        </td>
                        <td class="font-w600">{{number_format($projekat->budzet, 0, '.', ',')}} €</td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Преглед детаља пројекта" href="/predlog-projekta/{{$projekat->id}}">
                                <i class="si si-book-open"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@stop