@extends('admin.adminLayout')

@section('title')
    Насловна
@stop

@section('breadcrumbs')
<span class="breadcrumb-item active">Админ</span>
@stop

@section('heder-h1')
Насловна
@stop


@section('heder-h2')
Пристигло је укупно <a class="text-primary-light link-effect" href="/admin/projekti">{{count($projekti)}} предлога пројеката</a>.
@stop

@section('scriptsBottom')
    <script src="{{asset('assets/js/pages/be_pages_ecom_dashboard.js')}}"></script>
    <script>inicijalizujKontrolnuTablu('{!! addslashes(json_encode($datumi)) !!}', '{!! addslashes(json_encode($brojeviProjekata)) !!}', '{!! addslashes(json_encode($iznosi)) !!}');</script>
@endsection

@section('main')

<div class="row gutters-tiny">
    <!-- Earnings -->
    <div class="col-md-6">
        <a class="block block-rounded block-transparent bg-gd-elegance" href="/admin/projekti">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white">Пројекти</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Earnings -->

    <!-- Orders -->
    <div class="col-md-6">
        <a class="block block-rounded block-transparent bg-gd-dusk" href="/admin/obrazovne-ustanove">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white" >Образовне установе</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Orders -->

    <!-- New Customers -->
    <div class="col-md-6">
        <a class="block block-rounded block-transparent bg-gd-sea" href="admin/naucne-ustanove">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white">Научно-истраживачке установе</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- Earnings -->
    <div class="col-md-6">
        <a class="block block-rounded block-transparent bg-gd-elegance" href="/admin/oblasti-projekata">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white">Области пројеката</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Earnings -->

    <!-- Orders -->
    <div class="col-md-6">
        <a class="block block-rounded block-transparent bg-gd-dusk" href="/admin/oblasti-istrazivanja">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white" >Области истраживања</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Orders -->

    <!-- New Customers -->
    <div class="col-md-6">
        <a class="block block-rounded block-transparent bg-gd-sea" href="admin/kategorije-radova">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white">Категорије научних радова</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- END New Customers -->

</div>

<!-- END Statistics -->
<br/><br/>
<div class="row gutters-tiny">


    <!-- Orders Volume Chart -->
    <div class="col-md-6">
        <div class="block block-rounded block-mode-loading-refresh">
            <div class="block-header">
                <h3 class="block-title">
                    Пројекти - последњих 7 дана
                </h3>
                
            </div>
            <div class="block-content block-content-full bg-body-light text-center">
                <div class="row gutters-tiny">
                    <div class="col-12">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Укупно</div>
                        <div class="font-size-h3 font-w600">{{$totalWeekProjects}}</div>
                    </div>
                </div>
            </div>
            <div class="block-content block-content-full">
                <div class="pull-all">
                    <!-- Orders Chart Container -->
                    <canvas class="js-chartjs-ecom-dashboard-orders"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- END Orders Volume Chart -->
    <!-- Orders Earnings Chart -->
    <div class="col-md-6">
        <div class="block block-rounded block-mode-loading-refresh">
            <div class="block-header">
                <h3 class="block-title">
                    Буџети пројеката - последњих 7 дана
                </h3>
            </div>
            <div class="block-content block-content-full bg-body-light text-center">
                <div class="row gutters-tiny">
                    <div class="col-4">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Укупно</div>
                        <div class="font-size-h3 font-w600">{{number_format($totalWeekBudget, 0, '.', ',')}} €</div>
                    </div>
                    <div class="col-4">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Просечно по пројекту</div>
                        <div class="font-size-h3 font-w600 text-success">{{number_format($averageWeekBudget, 0, '.', ',')}} €</div>
                    </div>
                    <div class="col-4">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Највећи буџет</div>
                        <div class="font-size-h3 font-w600 text-danger">{{number_format($maxWeekBudget, 0, '.', ',')}} €</div>
                    </div>
                </div>
            </div>
            <div class="block-content block-content-full">
                <div class="pull-all">
                    <!-- Earnings Chart Container -->
                    <canvas class="js-chartjs-ecom-dashboard-earnings"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- END Orders Earnings Chart -->
</div>
<!-- END Orders Overview -->
@stop