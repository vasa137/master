@extends('admin.adminLayout')

@section('title')
    Образовне установе
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Админ</a>
    <span class="breadcrumb-item active">Образовне установе</span>
@stop

@section('heder-h1')
    Образовне установе
@stop


@section('heder-h2')
    Тренутно <a class="text-primary-light link-effect">{{count($aktivnihObrazovnihUstanova)}} активних образовних установа</a>.
@stop

@section('scriptsTop')
    <script src="{{asset('/js/adminObrazovneUstanove.js')}}"></script>
@endsection
@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaObrazovneUstanove.js')}}"></script>
@endsection

@section('main')
    <div class="row gutters-tiny">
        <!-- All Products -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-circle-o fa-2x text-info-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($aktivnihObrazovnihUstanova) + count($obrisanihObrazovnihUstanova)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Укупно образовних установа</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END All Products -->

        <!-- Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:showAvailable()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($aktivnihObrazovnihUstanova)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Активних</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->

        <!-- Out of Stock -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:showUnavailable()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-warning fa-2x text-danger-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{count($obrisanihObrazovnihUstanova)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Обрисаних</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Out of Stock -->

        <!-- Add Product -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="/admin/obrazovna-ustanova/-1">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-archive fa-2x text-success-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Додај нову образовну установу</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Add Product -->
    </div>
    <!-- END Overview -->

    <!-- Dynamic Table Full Pagination -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 id="obrazovne_ustanove-title" class="block-title">Образовне установе</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
            <table id="tabela-obrazovne_ustanove-aktivni" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th style="width:60%;">Назив</th>
                    <th class="d-none d-sm-table-cell text-center" style="width:32%;">Број особа</th>
                    <th class="text-center" style="width:20%;">Акција</th>
                </tr>
                </thead>
                <tbody>
                @foreach($aktivnihObrazovnihUstanova as $obrazovna_ustanova)
                    <tr>
                        <td class="font-w600">{{$obrazovna_ustanova->naziv}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$obrazovna_ustanova->broj_osoba}}</td>

                        <td class="text-center">
                            <a href="/admin/obrazovna-ustanova/{{$obrazovna_ustanova->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Измени образовну установу">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form method="POST" action="/admin/deleteEducationInstitution/{{$obrazovna_ustanova->id}}" style="display:inline">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Обриши образовну установу">
                                    <i class="fa fa-times"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <table id="tabela-obrazovne_ustanove-obrisani" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="display:none;">
                <thead>
                <tr>
                    <th style="width:60%;">Назив</th>
                    <th class="d-none d-sm-table-cell text-center" style="width:32%;">Број особа</th>
                    <th class="text-center" style="width:20%;">Акција</th>
                </tr>
                </thead>
                <tbody>
                @foreach($obrisanihObrazovnihUstanova as $obrazovna_ustanova)
                    <tr>
                        <td class="font-w600">{{$obrazovna_ustanova->naziv}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$obrazovna_ustanova->broj_osoba}}</td>

                        <td class="text-center">
                            <a href="/admin/obrazovna-ustanova/{{$obrazovna_ustanova->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Измени образовну установу">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form method="POST" action="/admin/restaurateEducationInstitution/{{$obrazovna_ustanova->id}}" style="display:inline">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Рестаурирај образовну установу">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@stop