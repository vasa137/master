@extends('admin.adminLayout')

@section('title')
    Области пројеката
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Админ</a>
    <span class="breadcrumb-item active">Области пројеката</span>
@stop

@section('heder-h1')
    Области пројеката
@stop


@section('heder-h2')
    Тренутно <a class="text-primary-light link-effect">{{count($aktivnihOblastiProjekata)}} активних области пројеката</a>.
@stop

@section('scriptsTop')
    <script src="{{asset('/js/adminOblastiProjekata.js')}}"></script>
@endsection
@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaOblastiProjekata.js')}}"></script>
@endsection

@section('main')
    <div class="row gutters-tiny">
        <!-- All Products -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-circle-o fa-2x text-info-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($aktivnihOblastiProjekata) + count($obrisanihOblastiProjekata)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Укупно области пројеката</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END All Products -->

        <!-- Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:showAvailable()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($aktivnihOblastiProjekata)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Активних</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->

        <!-- Out of Stock -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:showUnavailable()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-warning fa-2x text-danger-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{count($obrisanihOblastiProjekata)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Обрисаних</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Out of Stock -->

        <!-- Add Product -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="/admin/oblast-projekat/-1">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-archive fa-2x text-success-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Додај нову област пројекта</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Add Product -->
    </div>
    <!-- END Overview -->

    <!-- Dynamic Table Full Pagination -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 id="oblasti_projekata-title" class="block-title">Области пројеката</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
            <table id="tabela-oblasti_projekata-aktivni" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th style="width:60%;">Назив</th>
                    <th class="d-none d-sm-table-cell text-center" style="width:32%;">Број пројеката</th>
                    <th class="text-center" style="width:20%;">Акција</th>
                </tr>
                </thead>
                <tbody>
                @foreach($aktivnihOblastiProjekata as $oblast_projekat)
                    <tr>
                        <td class="font-w600">{{$oblast_projekat->naziv}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$oblast_projekat->broj_projekata}}</td>

                        <td class="text-center">
                            <a href="/admin/oblast-projekat/{{$oblast_projekat->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Измени област пројекта">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form method="POST" action="/admin/deleteProjectArea/{{$oblast_projekat->id}}" style="display:inline">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Обриши област пројекта">
                                    <i class="fa fa-times"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <table id="tabela-oblasti_projekata-obrisani" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="display:none;">
                <thead>
                <tr>
                    <th style="width:60%;">Назив</th>
                    <th class="d-none d-sm-table-cell text-center" style="width:32%;">Број пројеката</th>
                    <th class="text-center" style="width:20%;">Акција</th>
                </tr>
                </thead>
                <tbody>
                @foreach($obrisanihOblastiProjekata as $oblast_projekat)
                    <tr>
                        <td class="font-w600">{{$oblast_projekat->naziv}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$oblast_projekat->broj_projekata}}</td>

                        <td class="text-center">
                            <a href="/admin/oblast-projekat/{{$oblast_projekat->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Измени област пројекта">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form method="POST" action="/admin/restaurateProjectArea/{{$oblast_projekat->id}}" style="display:inline">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Рестаурирај област пројекта">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@stop