@extends('layout')

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/authentication.css')}}"/>
@endsection


@section('scriptsBottom')
    <script src="{{asset('js/resetPassword.js')}}"></script>
@endsection

@section('main')
    <div class="row">
        <div class="col m12 l6 offset-l3">
            <div class="card center-align mg front">

                <div class="card-content"  >
                    <span class="card-title">Промена лозинке</span>
                    <br/>
                    <form method="POST" action="{{ route('password.update') }}" onsubmit="return checkResetData();">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="input-field">
                            <input id="reset-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" type="text">
                            <label for="reset-email">Адреса е-поште</label>
                            <span id="reset-email-required" class="helper-text reset-error" style="display: none;">Морате унети адресу е-поште.</span>
                            <span id="reset-email-maxlen" class="helper-text reset-error" style="display: none;">Адреса е-поште не сме бити дужа од 254 карактера.</span>
                            <span id="reset-email-format" class="helper-text reset-error" style="display: none;">Морате унети коректан формат адресе е-поште.</span>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                     <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="input-field" >
                            <input id="reset-password" name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                            <label for="reset-password">Нова лозинка</label>
                            <span id="reset-password-required" class="helper-text reset-error" style="display: none;">Морате унети лозинку.</span>
                            <span id="reset-password-minlen" class="helper-text reset-error" style="display: none;">Лозинка мора садржати барем 6 карактера.</span>
                            <span id="reset-password-maxlen" class="helper-text reset-error" style="display: none;">Лозинка не сме бити дужа од 254 карактера.</span>
                        </div>
                        <div class="input-field" >
                            <input id="reset-confirm-password" name="password_confirmation" type="password">
                            <label for="reset-confirm-password">Потврда нове лозинке</label>
                            <span id="reset-confirm-password-required" class="helper-text reset-error" style="display: none;">Морате потврдити лозинку.</span>
                            <span id="reset-confirm-password-equal" class="helper-text reset-error" style="display: none;">Лозинка и потврда лозинке се не слажу.</span>
                        </div>

                        <button class="btn  waves-effect waves-light" type="submit" name="action">Потврди
                            <i class="material-icons right">send</i>
                        </button>
                        <br/><br/>
                    </form>
                </div><!--card-content-->

            </div>   <!--card-->
        </div>
    </div>

@endsection
