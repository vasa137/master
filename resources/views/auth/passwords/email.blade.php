@extends('layout')

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/authentication.css')}}"/>
@endsection


@section('scriptsBottom')
    <script src="{{asset('js/resetPassword.js')}}"></script>
@endsection

@section('main')
    <div class="row">
        <div class="col m12 l6 offset-l3">
            <div class="card center-align mg front">

                <div class="card-content">
                    <span class="card-title">Заборављена лозинка</span>
                    <br/>
                    <form method="POST" action="{{ route('password.email') }}" onsubmit="return checkEmailResetData()">
                        @csrf

                        <div class="input-field">
                            <input id="reset-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" type="text">
                            <label for="reset-email">Адреса е-поште</label>
                            <span id="reset-email-required" class="helper-text reset-error" style="display: none;">Морате унети адресу е-поште.</span>
                            <span id="reset-email-maxlen" class="helper-text reset-error" style="display: none;">Адреса е-поште не сме бити дужа од 254 карактера.</span>
                            <span id="reset-email-format" class="helper-text reset-error" style="display: none;">Морате унети коректан формат адресе е-поште.</span>
                            @if (session('status'))
                                <span id="reset-email-success" class="helper-text reset-success">Послали смо Вам линк за промену лозинке на наведену адресу е-поште.</span>
                            @endif

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                     <strong>Не постоји корисник са задатом адресом е-поште.</strong>
                                </span>
                            @endif
                        </div>

                        <button class="btn  waves-effect waves-light" type="submit" name="action">Пошаљи e-mail за промену лозинке
                            <i class="material-icons right">send</i>
                        </button>
                        <br/><br/>
                    </form>
                </div><!--card-content-->

            </div>   <!--card-->
        </div>
    </div>
@endsection
