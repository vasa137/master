@extends('layout')

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/authentication.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/authentication.js')}}"></script>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
@endsection

@section('main')
    <div class="row">
            <div class="row">
                <div class="col m12 l6 offset-l3">
                    <div class="wrapper" id="wp">
                        <div class="iwrapper" id="iwp">
                            <div class="card center-align mg front">
                                <div class="card-content ">
                                    <span class="card-title">Регистрација</span>
                                    <br/>
                                    <form method="POST" action="{{ route('register') }}" onsubmit="return checkRegisterData()">
                                        @csrf
                                        <div class="input-field">
                                            <input id="register-name" class="form-control{{ $errors->has('ime_prezime') ? ' is-invalid' : '' }}" name="ime_prezime" value="{{ old('ime_prezime') }}" type="text">
                                            <label for="register-name">Име и презиме</label>
                                            <span id="register-name-required" class="helper-text register-error" style="display: none;">Морате унети име и презиме.</span>
                                            <span id="register-name-maxlen" class="helper-text register-error" style="display: none;">Име и презиме не сме бити дуже од 254 карактера.</span>
                                            @if ($errors->has('ime_prezime'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('ime_prezime') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="input-field" >
                                            <input id="register-email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" type="text">
                                            <label for="register-email">Адреса е-поште</label>
                                            <span id="register-email-required" class="helper-text register-error" style="display: none;">Морате унети адресу е-поште.</span>
                                            <span id="register-email-maxlen" class="helper-text register-error" style="display: none;">Адреса е-поште не сме бити дужа од 254 карактера.</span>
                                            <span id="register-email-format" class="helper-text register-error" style="display: none;">Морате унети коректан формат адресе е-поште.</span>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Већ постоји кориснички налог регистрован на унету адресу е-поште.</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="input-field" >
                                            <input id="register-password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" type="password">
                                            <label for="register-password">Лозинка</label>
                                            <span id="register-password-required" class="helper-text register-error" style="display: none;">Морате унети лозинку.</span>
                                            <span id="register-password-minlen" class="helper-text register-error" style="display: none;">Лозинка мора садржати барем 6 карактера.</span>
                                            <span id="register-password-maxlen" class="helper-text register-error" style="display: none;">Лозинка не сме бити дужа од 254 карактера.</span>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="input-field" >
                                            <input id="register-confirm-password" name="password_confirmation"  class="form-control" type="password">
                                            <label for="register-confirm-password">Потврда лозинке</label>
                                            <span id="register-confirm-password-required" class="helper-text register-error" style="display: none;">Морате потврдити лозинку.</span>
                                            <span id="register-confirm-password-equal" class="helper-text register-error" style="display: none;">Лозинка и потврда лозинке се не слажу.</span>
                                        </div>

                                        <!-- DA BI funkcija env radila mora se pokrenuti artisan config:clear -->
                                        <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}" id="captcha"  style="display: flex; justify-content: center;">
                                            <div class="g-recaptcha" data-theme="light" data-sitekey="{{config('app.re_cap_site')}}" style="overflow:hidden;"></div>
                                            @if ($errors->has('g-recaptcha-response'))
                                                <br/>
                                                <span class="invalid-feedback" role="alert" style="display:block;">
                                                    <strong>Морате потврдити да нисте робот.</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <br/>
                                        <button class="btn  waves-effect waves-light" type="submit" name="action">Даље
                                            <i class="material-icons right">send</i>
                                        </button>

                                        <br/><br/>
                                    </form>

                                    <a id="bt1" class="halfway-fab waves-effect waves-light login-link">Већ имате налог?</a>
                                </div><!--card-content-->

                            </div>   <!--card-->
                            <div class="card center-align mg back">
                                <div class="card-content">
                                    <span class="card-title">Пријава</span>

                                    <br/>
                                    <form method="POST" action="{{ route('login') }}" onsubmit="return checkLoginData();">
                                        @csrf
                                        <div class="input-field">
                                            <input id="login-email" name="email" type="text">
                                            <label for="login-email">Адреса е-поште</label>
                                            <span id="login-email-required" class="helper-text login-error" style="display: none;">Морате унети адресу е-поште.</span>
                                            <span id="login-email-maxlen" class="helper-text login-error" style="display: none;">Адреса е-поште не сме бити дужа од 254 карактера.</span>
                                            <span id="login-email-format" class="helper-text login-error" style="display: none;">Морате унети коректан формат е-поште.</span>
                                        </div>
                                        <div class="input-field" >
                                            <input id="login-password" name="password" type="password">
                                            <label for="login-password">Лозинка</label>
                                            <span id="login-password-required" class="helper-text login-error" style="display: none;">Морате унети лозинку.</span>
                                            <span id="login-password-maxlen" class="helper-text login-error" style="display: none;">Лозинка не сме бити дужа од 254 карактера.</span>
                                            <br/>

                                        </div>
                                        <div class="input-field" style="display:flex;">
                                            <p>
                                                <label>
                                                    <input type="checkbox" class="filled-in" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                                                    <span >Запамти ме</span>
                                                </label>
                                            </p>

                                        </div>
                                        <a style="display:flex; margin-bottom:10px;" href="{{ route('password.request') }}" class="halfway-fab waves-effect waves-light login-link">Заборавили сте лозинку?</a>

                                        <button class="btn  waves-effect waves-light" type="submit" name="action">Даље
                                            <i class="material-icons right">send</i>
                                        </button>
                                        <br/><br/>
                                    </form>
                                    <a id="bt2" class="halfway-fab waves-effect waves-light login-link">Немате налог?</a>

                                </div><!--card-content-->


                            </div>   <!--card-->
                        </div> <!--iwrapper-->
                    </div> <!--wrapper-->
                </div> <!--col-->
            </div>  <!--row-->

    </div><!--Frow-->

@endsection
