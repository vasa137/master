@extends('layout')

@section('scriptsBottom')
    <script src="{{asset('js/mojiProjekti.js')}}"></script>
@endsection

@section('main')
    <div class="row">
        <div class="col s12">
            <div class="card darken-1">
                <div class="card-content">
                    <span class="card-title" style="padding-bottom:15px;">Моји предлози пројеката</span>
                    <hr style="margin:0;">
                    <div class="card-inner" style="padding:0;">
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs">
                                <li class="tab col s6"><a class="actiove" href="#drafts">Снимљени</a></li>
                                <li class="tab col s6"><a href="#submitted">Послати</a></li>
                            </ul>
                        </div>
                        <div id="drafts" class="col s10 offset-s1">
                            <div class="row">
                                <br/>
                                @if(count($drafts) > 0)
                                    <ul class="collection">
                                        @foreach($drafts as $projekat)
                                            <li class="collection-item avatar">
                                                <i class="material-icons circle" style="background:royalblue; color:white;">class</i>
                                                <a href="/predlog-projekta/{{$projekat->id}}" style="color:black;">
                                                    <span class="title">#{{$projekat->id}} - {{$projekat->naslov}}</span>
                                                    <p>{{$projekat->osobe[0]->ime}}&nbsp;{{$projekat->osobe[0]->prezime}} <br>
                                                        @if($projekat->budzet != null) {{number_format($projekat->budzet, 0, '.', ',')}} € @endif
                                                        <br>
                                                        {{date('d.m.Y. H:i:s',strtotime($projekat->updated_at))}}
                                                    </p>
                                                </a>
                                                <a href="/predlog-projekta/{{$projekat->id}}" class="secondary-content tooltipped" data-position="left" data-tooltip="Наставите унос информација о предлогу пројекта"><i class="material-icons">edit</i></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @else
                                    <h6>Нема снимљених пројеката.</h6>
                                @endif
                            </div>
                        </div>
                        <div id="submitted" class="col s10 offset-s1">
                            <div class="row">
                            <br/>
                            @if(count($submitted) > 0)
                                <ul class="collection">
                                    @foreach($submitted as $projekat)
                                        <li class="collection-item avatar">
                                            <a href="/predlog-projekta/{{$projekat->id}}" style="color:black;">
                                                <i class="material-icons circle" style="background:royalblue; color:white;">class</i>
                                                <span class="title">#{{$projekat->id}} - {{$projekat->naslov}}</span>
                                                <p>Руководилац - {{$projekat->osobe[0]->ime}}&nbsp;{{$projekat->osobe[0]->prezime}} <br>
                                                    {{number_format($projekat->budzet, 0, '.', ',')}} €
                                                    <br>
                                                    {{date('d.m.Y. H:i:s',strtotime($projekat->updated_at))}}
                                                </p>
                                            </a>
                                            <a href="/predlog-projekta/{{$projekat->id}}" class="secondary-content tooltipped" data-position="left" data-tooltip="Погледајте предлог пројекта који сте предали"><i class="material-icons">remove_red_eye</i></a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <h6>Нема послатих пројеката.</h6>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
