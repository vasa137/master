<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Master</title>
    <script src="https://kit.fontawesome.com/e1245a0e9c.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="{{asset('css/materialize.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link rel="icon" href="{{asset('images/favicon.png')}}">

    @yield('scriptsTop')
</head>
<body>
<div class="fix-section">

<nav role="navigation">
    <div class="nav-wrapper container">

        <a id="logo-container" href="/" class="brand-logo" >
            <img style="height:45px; width:200px; margin-top:10px;" src="{{asset('images/logo.png')}}" alt="logo"/>
        </a>

        <ul class="right hide-on-med-and-down">
            @if(Auth::check() and !Auth::user()->admin)
                <li>
                    <a href="/moji-projekti">
                        Моји пројекти
                    </a>
                </li>
                <li>
                    <a href="/predlog-projekta/-1">
                       Нови пројекат
                    </a>
                </li>

            @endif

            @if(Auth::check() and Auth::user()->admin)
                <li>
                    <a href="/admin">
                        Админ панел
                    </a>
                </li>
            @endif

            @if(Auth::check())
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Одјава
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            @endif
</ul>

<ul id="nav-mobile" class="sidenav">
@if(Auth::check())
    <li>
        <a href="/predlog-projekta/-1">
            Нови предлог пројекта
        </a>
    </li>
    <li>
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
            Одјава
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
@endif
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
</div>
</nav>
<!--Main Navigation-->

<div class="container" style="min-height:88.2vh;">
<div class="section">

@yield('main')

</div>
</div>

<footer class="page-footer" style="padding-top: 0;">
<div class="footer-copyright">
<div class="container" style="display: flex; align-items: center; justify-content: center;">
<i class="material-icons">copyright</i>&nbsp; Портал за пријаву научних пројеката - Никола Васовић - 2019  &nbsp;<i class="material-icons">copyright</i>
</div>
</div>
</footer>
<!--/.Footer-->
<script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>

<script src="{{asset('js/materialize.min.js')}}"></script>

<script src="{{asset('js/moment.js')}}"></script>

<script>
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
</script>

@yield('scriptsBottom')
</div>
</body>
</html>
