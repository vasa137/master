@extends('layout')

@section('scriptsBottom')
    <script src="{{asset('js/prijava.js')}}"></script>
    @if($edit)
        <script> initEditProject('{!! addslashes(json_encode($projekat)) !!}'); </script>
    @endif
@endsection

@section('main')
    @include('include.modalEducation')
    @include('include.modalJob')
    @include('include.modalReference')
    @include('include.modalSaveSuccessful')
    @include('include.modalSaveFailed')
    @include('include.modalLoader')
    <div class="row" >
        <form class="col s12">
            <div id="page-1">


                <div class="row">
                    <div class="col s12">
                        <div class="card-panel" style="background:#186ec7;text-align: center; font-size:1.1em;">
                            <span class="white-text">
                                @if($edit)
                                    Предлог пројекта #{{$projekat->id}} @if(Auth::user()->admin) - {{$projekat->user->ime_prezime}} ({{$projekat->user->email}}) @endif
                                @else
                                    Нови предлог пројекта
                                @endif
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <div class="card darken-1">
                            <div class="card-content">
                                <span class="card-title">Опште информације о предлогу пројекта</span>
                                <hr>
                                <div class="card-inner">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="projectTitle" type="text" @if($edit) value="{{$projekat->naslov}}" @endif>
                                            <label for="projectTitle">Наслов пројекта <span class="asterisk">*</span></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <textarea id="abstractTA" class="materialize-textarea" data-length="2000">@if($edit){{$projekat->apstrakt}}@endif</textarea>
                                            <label for="abstractTA">Апстракт <span class="asterisk">*</span>&nbsp;<i style="cursor:pointer" class="fas fa-question-circle  tooltipped" data-position="right" data-tooltip="Максимум 2000 словних знакова, укључујучи бланко знаке"> </i></label>
                                            <span class="helper-text" data-error="Морате унети апстракт до 2000 словних знакова, укључујући и размаке."></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12 m7 l5">
                                            <select id="areaSelect">
                                                <option disabled selected></option>
                                                @foreach($adminPodaci->oblastiProjekta as $oblastProjekta)
                                                    <option value="{{$oblastProjekta->id}}" @if($edit and $oblastProjekta->id == $projekat->id_oblast_projekat) selected @endif>{{$oblastProjekta->naziv}}</option>
                                                @endforeach
                                            </select>
                                            <label for="areaSelect">Област пројекта <span class="asterisk">*</span>&nbsp;<i style="cursor:pointer" class="fas fa-question-circle  tooltipped" data-position="right" data-tooltip="Бира се једна од понуђених области, којој предлог пројекта највише припада"> </i></label>
                                            <span class="helper-text" data-error="Морате изабрати област пројекта"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12 m7 l5">
                                            <i class="material-icons prefix">euro_symbol</i>
                                            <input id="budgetText" type="text" @if($edit and $projekat->budzet != null) value="{{number_format($projekat->budzet,0, '.', ',')}}" @endif>
                                            <label for="budgetText" >Укупан буџет пројекта <span class="asterisk">*</span>&nbsp;<i style="cursor:pointer" class="fas fa-question-circle  tooltipped" data-position="right" data-tooltip="Уноси се сума у еврима, до 200.000€"> </i></label>
                                            <span class="helper-text" data-error="Морате унети ненегативну цифру до 200.000€"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <div class="card darken-1">
                            <div class="card-content">
                                <span class="card-title">Информације о руководиоцу пројекта</span>
                                <hr>

                                @include('include.people', ['peopleId' => 0])
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <div class="card darken-1">
                            <div class="card-content">
                                <span class="card-title" style="padding-bottom:15px;">Информације о учесницима пројекта</span>
                                <hr style="margin:0;">
                                <ul class="tabs tabs-fixed-width tab-demo z-depth-1">
                                    <li id="member-tabheading-1" class="tab"><a class="active" href="#member1">Учесник 1</a></li>
                                    <li id="member-tabheading-2" class="tab"><a href="#member2">Учесник 2</a></li>
                                    <li id="member-tabheading-3" class="tab"><a href="#member3">Учесник 3</a></li>
                                    <li id="member-tabheading-4" class="tab"><a href="#member4">Учесник 4</a></li>
                                    <li id="member-tabheading-5" class="tab"><a href="#member5">Учесник 5</a></li>
                                </ul>

                                @include('include.people', [ 'peopleId' => 1])
                                @include('include.people', [ 'peopleId' => 2])
                                @include('include.people', [ 'peopleId' => 3])
                                @include('include.people', [ 'peopleId' => 4])
                                @include('include.people', [ 'peopleId' => 5])
                            </div>
                        </div>
                    </div>
                </div>


                <a class="waves-effect waves-light btn teal" href="javascript:nextPage()"><i class="material-icons right">arrow_forward</i>Даље&nbsp;</a>

                @if(!$edit or !$projekat->poslata)
                <a class="waves-effect waves-light btn tooltipped" href="javascript:saveDraft('{{$id}}')" data-position="bottom" data-tooltip="Снимају се тренутно унети подаци и могуће је каснијим приступом систему наставити унос података"><i class="material-icons left">save</i>Сними&nbsp;</a>
                @endif
            </div>

            <div id="page-2" style="display: none;">
                <div class="row">
                    <div class="col s12">
                        <div class="card darken-1">
                            <div class="card-content">
                                <span class="card-title">Пропратне датотеке</span>
                                <hr>
                                <div class="card-inner">
									@if($edit and $projekat->poslata)
										<div class="row">
											<div class="input-field col s12" style="margin:0px;">
												<h6 style="margin:0px;"><a href="/preuzimanje-datoteka/{{$id}}" target="_blank" id="download-files" style="display: flex; align-items: center;justify-content: center;"><i class="material-icons">file_download</i>Преузимање свих датотека</a></h6>
											</div>
										</div>
									@endif
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>Опис пројекта <span class="asterisk">*</span></span>
                                                    <input onchange="uploadProjectFile(this, 0, 'opis', ['pdf']);" accept="application/pdf" type="file">
                                                </div>

                                                <div class="file-path-wrapper">
                                                    <input id="file-0-filepath" class="file-path validate" type="text" @if($edit and count($projekat->filesDescriptor->projectFiles[0]) == 1) value="{{$projekat->filesDescriptor->projectFiles[0][0]}}" @endif />

                                                    <span id="file-0-extension" class="helper-text file-error" style="display: none;">Приложени фајл мора бити у PDF формату.</span>
                                                    <span id="file-0-size" class="helper-text file-success" style="display: none;">Приложени фајл мора бити мањи од 10MB.</span>
                                                    <span id="file-0-success" class="helper-text file-success" style="display: none;">Приложени фајл je успешно пребачен на сервер.</span>
                                                </div>

                                            </div>

                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>Буџет <span class="asterisk">*</span></span>
                                                    <input onchange="uploadProjectFile(this, 1, 'budzet', ['xlsx', 'xls', 'csv']);" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" type="file">
                                                </div>

                                                <div class="file-path-wrapper">
                                                    <input id="file-1-filepath" class="file-path validate" type="text" @if($edit and count($projekat->filesDescriptor->projectFiles[1]) == 1) value="{{$projekat->filesDescriptor->projectFiles[1][0]}}" @endif />
                                                    <span id="file-1-extension" class="helper-text file-error" style="display: none;">Приложени фајл мора бити у неком од Excel формата (xlsx, xls, csv).</span>
                                                    <span id="file-1-size" class="helper-text file-error" style="display: none;">Приложени фајл мора бити мањи од 10MB.</span>
                                                    <span id="file-1-success" class="helper-text file-success" style="display: none;">Приложени фајл je успешно пребачен на сервер.</span>
                                                </div>
                                            </div>

                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>Гантограм <span class="asterisk">*</span></span>
                                                    <input onchange="uploadProjectFile(this, 2, 'gantogram', ['pdf']);" accept="application/pdf" type="file">
                                               </div>

                                                <div class="file-path-wrapper">
                                                    <input id="file-2-filepath" class="file-path validate" type="text" @if($edit and count($projekat->filesDescriptor->projectFiles[2]) == 1) value="{{$projekat->filesDescriptor->projectFiles[2][0]}}" @endif>
                                                    <span id="file-2-extension" class="helper-text file-error" style="display: none;">Приложени фајл мора бити у PDF формату.</span>
                                                    <span id="file-2-size" class="helper-text file-error" style="display: none;">Приложени фајл мора бити мањи од 10MB.</span>
                                                    <span id="file-2-success" class="helper-text file-success" style="display: none;">Приложени фајл je успешно пребачен на сервер.</span>
                                                </div>
                                            </div>

                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>Презентација пројекта <span class="asterisk">*</span></span>
                                                    <input onchange="uploadProjectFile(this, 3, 'prezentacija', ['pdf']);" accept="application/pdf" type="file">
                                                </div>

                                                <div class="file-path-wrapper">
                                                    <input id="file-3-filepath" class="file-path validate" type="text" @if($edit and count($projekat->filesDescriptor->projectFiles[3]) == 1) value="{{$projekat->filesDescriptor->projectFiles[3][0]}}" @endif>
                                                    <span id="file-3-extension" class="helper-text file-error" style="display: none;">Приложени фајл мора бити у PDF формату.</span>
                                                    <span id="file-3-size" class="helper-text file-error" style="display: none;">Приложени фајл мора бити мањи од 10MB.</span>
                                                    <span id="file-3-success" class="helper-text file-success" style="display: none;">Приложени фајл je успешно пребачен на сервер.</span>

                                                </div>
                                            </div>
                                            <div class="row" style="display: flex; align-items: center;justify-content: center;">
                                                <div class="file-field input-field col s11">
                                                    <div class="btn">
                                                        <span>Изјаве <span class="asterisk">*</span></span>
                                                        <input onchange="uploadMultipleStatementFiles(this, 4, ['pdf']);" accept="application/pdf" type="file" multiple >
                                                    </div>

                                                    <div class="file-path-wrapper">
                                                        <input id="file-4-filepath" class="file-path validate" type="text" @if($edit and count($projekat->filesDescriptor->projectFiles[4]) > 0) value="<?php for($i = 0; $i < count($projekat->filesDescriptor->projectFiles[4]); $i++){?>{{$projekat->filesDescriptor->projectFiles[4][$i]}}@if($i < count($projekat->filesDescriptor->projectFiles[4]) -1), @endif <?php } ?>" @endif>
                                                        <span id="file-4-extension" class="helper-text file-error" style="display: none;">Приложени фајлoви морају бити у PDF формату.</span>
                                                        <span id="file-4-size" class="helper-text file-error" style="display: none;">Приложени фајлови морају појединачно бити мањи од 5MB.</span>
                                                        <span id="file-4-count" class="helper-text file-error" style="display: none;">Приложени фајлoви морају бити у PDF формату.</span>
                                                        <span id="file-4-success" class="helper-text file-success" style="display: none;">Приложени фајлови су успешно пребачени на сервер.</span>
                                                    </div>
                                                </div>
                                                <div class="col s1" style="text-align: center;">
                                                     <i style="cursor:pointer" class="fas fa-question-circle  tooltipped" data-position="top" data-tooltip="Можете приложити највише 6 датотека са изјавама"> </i>
                                                </div>
                                            </div>
                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>Библиографија руководиоца <span class="asterisk">*</span></span>
                                                    <input onchange="uploadPeopleBibliographyFile(this, 0, 'bibliografija', ['pdf']);" accept="application/pdf" type="file">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input id="bibliography-0-filepath" class="file-path validate" type="text" @if($edit and count($projekat->filesDescriptor->bibliography[0]) == 1) value="{{$projekat->filesDescriptor->bibliography[0][0]}}" @endif>
                                                    <span id="bibliography-0-extension" class="helper-text file-error" style="display: none;">Приложени фајл мора бити у PDF формату.</span>
                                                    <span id="bibliography-0-size" class="helper-text file-error" style="display: none;">Приложени фајл мора бити мањи од 5MB.</span>
                                                    <span id="bibliography-0-success" class="helper-text file-success" style="display: none;">Приложени фајл је успешно пребачен на сервер.</span>
                                                </div>
                                            </div>

                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>Биографија руководиоца <span class="asterisk">*</span></span>
                                                    <input onchange="uploadPeopleBiographyFile(this, 0, 'biografija', ['pdf']);" accept="application/pdf" type="file">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input id="biography-0-filepath" class="file-path validate" type="text" @if($edit and count($projekat->filesDescriptor->biography[0]) == 1) value="{{$projekat->filesDescriptor->biography[0][0]}}" @endif>
                                                    <span id="biography-0-extension" class="helper-text file-error" style="display: none;">Приложени фајл мора бити у PDF формату.</span>
                                                    <span id="biography-0-size" class="helper-text file-error" style="display: none;">Приложени фајл мора бити мањи од 5MB.</span>
                                                    <span id="biography-0-success" class="helper-text file-success" style="display: none;">Приложени фајл је успешно пребачен на сервер.</span>
                                                </div>
                                            </div>

                                            <br/>
                                            <ul id="biography-collection" class="collection">

                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!$edit or !$projekat->poslata)
                <p>
                    <label>
                        <input id="conditions-CB" type="checkbox" />
                        <span>Слажем се са условима пријаве</span>
                    </label>
                </p>

                <br/>
                @endif
                <a class="waves-effect waves-light btn red" href="javascript:prevPage()"><i class="material-icons left">arrow_back</i>Назад&nbsp;</a>

                @if(!$edit or  !$projekat->poslata)
                <a class="waves-effect waves-light btn tooltipped"  href="javascript:saveDraft('{{$id}}')"  data-position="bottom" data-tooltip="Снимају се тренутно унети подаци и могуће је каснијим приступом систему наставити унос података"><i class="material-icons left">save</i>Сними&nbsp;</a>

                <a class="waves-effect waves-light btn teal tooltipped" href="javascript:submitProject('{{$id}}')" data-position="bottom" data-tooltip="Пријава се прослеђује администратору и није могуће каснијим приступом систему наставити унос података" style="float:right" ><i class="material-icons left">send</i>Пошаљи</a>
                @endif
            </div>

            <ul class="collection with-header" id="draftErrors-collection" style="display:none;">


            </ul>
        </form>
    </div>
@endsection
