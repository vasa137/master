<div id="member{{$peopleId}}" class="card-inner">
    @if($peopleId > 0)
    <p>
        <label>
            <input id="member-{{$peopleId}}-CB" type="checkbox" class="filled-in" onchange="memberCheckBoxChanged({{$peopleId}})"/>
            <span>Региструј учесника </span>
        </label>
    </p>
    @endif
    <div id="member-{{$peopleId}}-content">
    <div class="row">
        <div class="input-field col s12 m4">
            <input id="{{$peopleId}}-firstName" type="text" @if($edit and isset($projekat->osobe[$peopleId])) value="{{$projekat->osobe[$peopleId]->ime}}" @endif>
            <label for="{{$peopleId}}-firstName">Име <span class="asterisk">*</span></label>
        </div>
        <div class="input-field col s12 m4">
            <input id="{{$peopleId}}-lastName" type="text" @if($edit and isset($projekat->osobe[$peopleId])) value="{{$projekat->osobe[$peopleId]->prezime}}" @endif>
            <label for="{{$peopleId}}-lastName">Презиме <span class="asterisk">*</span></label>
        </div>
        <div class="input-field col s12 m4">
            <input id="{{$peopleId}}-birth" type="text" class="datepicker" @if($edit and isset($projekat->osobe[$peopleId]) and $projekat->osobe[$peopleId]->datum_rodjenja != null) value="{{date('d.m.Y.',strtotime($projekat->osobe[$peopleId]->datum_rodjenja))}}" @endif>
            <label for="{{$peopleId}}-birth">Датум рођења <span class="asterisk">*</span></label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <select id="{{$peopleId}}-researchArea" multiple onchange="memberResearchAreaChanged('{{$peopleId}}')">
                <option disabled></option>
                @foreach($adminPodaci->oblastiIstrazivanja as $oblastIstrazivanja)
                    <option value="{{$oblastIstrazivanja->id}}" @if($edit and isset($projekat->osobe[$peopleId]) and in_array($oblastIstrazivanja, $projekat->osobe[$peopleId]->oblastiIstrazivanja)) selected @endif>{{$oblastIstrazivanja->naziv}}</option>
                @endforeach
            </select>
            <label for="{{$peopleId}}-researchArea">Oбласт(и) истраживања <span class="asterisk">*</span> <i style="cursor:pointer" class="fas fa-question-circle  tooltipped" data-position="right" data-tooltip="Бира се минимум једна, а максимум пет области истраживања"> </i></label>
            <span id="{{$peopleId}}-researchArea-maxlen" class="helper-text people-error" style="display: none;">Не можете селектовати више од 5 области истраживања.</span>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <ul class="collapsible">
                <li class="active">
                    <div class="collapsible-header"><i class="material-icons">school</i>Образовање</div>
                    <div class="collapsible-body">
                        <div class="col s12">
                            <ul class="collection" id="educationCollection-{{$peopleId}}" >
                            </ul>
                        </div>
                        <div>
                            <a class="btn-floating btn-medium waves-effect waves-light tooltipped add-button" href="javascript:newEducation({{$peopleId}});" data-position="right" data-tooltip="Додај нове податке о образовању"><i class="material-icons">add</i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">work</i>Запослење</div>
                    <div class="collapsible-body">
                        <div class="col s12">
                            <ul class="collection" id="jobCollection-{{$peopleId}}" >
                            </ul>
                        </div>
                        <div>
                            <a class="btn-floating btn-medium waves-effect waves-light tooltipped add-button" href="javascript:newJob({{$peopleId}});" data-position="right" data-tooltip="Додај нове податке о запослењу"><i class="material-icons">add</i></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">book</i>Референце</div>
                    <div class="collapsible-body">
                        <div class="col s12">
                            <ul class="collection" id="referenceCollection-{{$peopleId}}" >
                            </ul>
                        </div>
                        <div>
                            <a id="{{$peopleId}}-reference-add-button" class="btn-floating btn-medium waves-effect waves-light tooltipped add-button" href="javascript:newReference({{$peopleId}});" data-position="right" data-tooltip="Додај нову референцу (максимум 5 референци)"><i class="material-icons">add</i></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    </div>
</div>