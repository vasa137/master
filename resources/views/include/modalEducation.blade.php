<div id="modal-education" class="modal">
    <div class="modal-content">
        <h4>Образовање</h4>
        <br/>
        <input type="hidden" id="peopleId"/>
        <input type="hidden" id="educationId"/>
        <div class="row">
            <div class="input-field col s12">
                <select id="universityNameSelect">
                    <option disabled selected></option>
                    @foreach($adminPodaci->obrazovneUstanove as $obrazovnaUstanova)
                        <option value="{{$obrazovnaUstanova->id}}">{{$obrazovnaUstanova->naziv}}</option>
                    @endforeach
                </select>
                <label for="universityNameSelect">Образовна установа <span class="asterisk">*</span></label>
                <span id="universityNameSelect-required" class="helper-text education-error" style="display: none;">Морате изабрати образовну установу.</span>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <select id="studiesTypeSelect">
                    <option disabled selected></option>
                    @foreach($adminPodaci->stepeniStudija as $stepenStudija)
                        <option value="{{$stepenStudija->id}}">{{$stepenStudija->naziv}}</option>
                    @endforeach
                </select>
                <label for="studiesTypeSelect">Врста студија <span class="asterisk">*</span></label>
                <span id="studiesTypeSelect-required" class="helper-text education-error" style="display: none;">Морате изабрати степен студија.</span>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="universityTitle" type="text">
                <label class="label-to-activate" for="universityTitle">Звање <span class="asterisk">*</span></label>
                <span id="universityTitle-required" class="helper-text education-error" style="display: none;">Морате унети стечено звање.</span>
                <span id="universityTitle-maxlen" class="helper-text education-error" style="display: none;">Назив стеченог звања не сме садржати више од 255 карактера.</span>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="graduationDate" type="text" class="datepicker">
                <label class="label-to-activate" for="graduationDate">Датум стицања звања <span class="asterisk">*</span></label>
                <span id="graduationDate-required" class="helper-text education-error" style="display: none;">Морате унети датум стицања звања.</span>
                <span id="graduationDate-future" class="helper-text education-error" style="display: none;">Датум стицања звања не сме бити у будућности.</span>
            </div>
        </div>
        <div class="modal-footer">
            @if($edit and $projekat->poslata)
            <a href="#!" class="modal-close waves-effect waves-red btn-flat">Затвори</a>
            @else
            <a href="#!" class="modal-close waves-effect waves-red btn-flat">Откажи</a>
            <a href="javascript: saveEducation()" class="waves-effect waves-green btn-flat">Сачувај</a>
            @endif
        </div>
    </div>
</div>
<div id="modal-delete-education" class="modal">
    <div class="modal-content">
        <h4>Образовање</h4>
        <br/>
        <input type="hidden" id="deleteEducationId"/>
        <p>Да ли сте сигурни да желите да обришете податке о изабраном образовању?</p>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red btn-flat">Откажи</a>
            <a href="javascript: deleteEducation()" class="modal-close waves-effect waves-green btn-flat">Обриши</a>
        </div>
    </div>
</div>