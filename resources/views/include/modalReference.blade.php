<div id="modal-reference" class="modal">
    <div class="modal-content">
        <h4>Референца</h4>
        <br/>
        <input type="hidden" id="peopleId"/>
        <input type="hidden" id="referenceId"/>

        <div class="row">
            <div class="input-field col s12 m4">
                <input id="referenceWorkIdentifier" type="text">
                <label class="label-to-activate" for="referenceWorkIdentifier">Идентификатор <span class="asterisk">*</span></label>
                <span id="referenceWorkIdentifier-required" class="helper-text reference-error" style="display: none;">Морате унети идентификатор рада.</span>
                <span id="referenceWorkIdentifier-maxlen" class="helper-text reference-error" style="display: none;">Идентификатор рада не сме садржати више од 255 карактера.</span>
            </div>
            <div class="input-field col s12 m8">
                <input id="referenceWorkTitle" type="text">
                <label class="label-to-activate" for="referenceWorkTitle">Назив рада <span class="asterisk">*</span></label>
                <span id="referenceWorkTitle-required" class="helper-text reference-error" style="display: none;">Морате унети назив рада.</span>
                <span id="referenceWorkTitle-maxlen" class="helper-text reference-error" style="display: none;">Назив рада не сме садржати више од 255 карактера.</span>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <select id="referenceWorkCategorySelect">
                    <option disabled selected></option>
                    @foreach($adminPodaci->kategorijeRada as $kategorijaRada)
                        <option value="{{$kategorijaRada->id}}">{{$kategorijaRada->naziv}}</option>
                    @endforeach
                </select>
                <label for="referenceWorkCategorySelect">Категорија рада <span class="asterisk">*</span></label>
                <span id="referenceWorkCategorySelect-required" class="helper-text reference-error" style="display: none;">Морате одабрати категорију рада.</span>
            </div>
        </div>
        <div class="row">

            <div class="input-field col s12">
                <input id="referenceWorkMagazines" type="text">
                <label class="label-to-activate" for="referenceWorkMagazines">Места објављивања <span class="asterisk">*</span></label>
                <span id="referenceWorkMagazines-required" class="helper-text reference-error" style="display: none;">Морате унети часописе у којима је рад објављен.</span>
                <span id="referenceWorkMagazines-maxlen" class="helper-text reference-error" style="display: none;">Места објављивања рада не смеју садржати у збиру више од 1000 карактера.</span>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <input id="referenceWorkSigners" type="text">
                <label class="label-to-activate" for="referenceWorkSigners">Потписници <span class="asterisk">*</span> <i style="cursor:pointer" class="fas fa-question-circle  tooltipped" data-position="right" data-tooltip="Навести имена и презимена потписника научног рада (размакнути зарезом)"> </i></label>
                <span id="referenceWorkSigners-required" class="helper-text reference-error" style="display: none;">Морате унети имена и презимена потписника рада.</span>
                <span id="referenceWorkSigners-maxlen" class="helper-text reference-error" style="display: none;">Идентитети потписника рада не смеју садржати у збиру више од 255 карактера.</span>
            </div>
        </div>


        <div class="modal-footer">
            @if($edit and $projekat->poslata)
            <a href="#!" class="modal-close waves-effect waves-red btn-flat">Затвори</a>
            @else
            <a href="#!" class="modal-close waves-effect waves-red btn-flat">Откажи</a>
            <a href="javascript: saveReference()" class="waves-effect waves-green btn-flat">Сачувај</a>
            @endif
        </div>
    </div>
</div>
<div id="modal-delete-reference" class="modal">
    <div class="modal-content">
        <h4>Референца</h4>
        <br/>
        <input type="hidden" id="deleteReferenceId"/>
        <p>Да ли сте сигурни да желите да обришете изабрану референцу?</p>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red btn-flat">Откажи</a>
            <a href="javascript: deleteReference()" class="modal-close waves-effect waves-green btn-flat">Обриши</a>
        </div>
    </div>
</div>