<div id="modal-job" class="modal">
    <div class="modal-content">
        <h4>Запослење</h4>
        <br/>
        <input type="hidden" id="peopleId"/>
        <input type="hidden" id="jobId"/>

        <div class="row">
            <div class="input-field col s12 m6">
                <input id="jobStartDate" type="text" class="datepicker">
                <label class="label-to-activate" for="jobStartDate">Датум почетка рада <span class="asterisk">*</span></label>
                <span id="jobStartDate-required" class="helper-text job-error" style="display: none;">Морате унети датум почетка рада.</span>
                <span id="jobStartDate-future" class="helper-text job-error" style="display: none;">Датум почетка рада не може бити у будућности.</span>
                <span id="jobStartDate-after" class="helper-text job-error" style="display: none;">Датум почетка рада не може бити после датума краја рада.</span>
            </div>

            <div class="input-field col s12 m6">
                <input id="jobEndDate" type="text" class="datepicker">
                <label class="label-to-activate" for="jobEndDate">Датум краја рада </label>
                <span id="jobEndDate-future" class="helper-text job-error" style="display: none;">Датум краја рада не може бити у будућности.</span>
                <span id="jobEndDate-before" class="helper-text job-error" style="display: none;">Датум краја рада не може бити пре датума почетка рада.</span>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <select id="institutionNameSelect">
                    <option disabled selected></option>
                    @foreach($adminPodaci->naucneUstanove as $naucnaUstanova)
                        <option value="{{$naucnaUstanova->id}}">{{$naucnaUstanova->naziv}}</option>
                    @endforeach
                </select>
                <label for="institutionNameSelect">Научно-истраживачка установа <span class="asterisk">*</span></label>
                <span id="institutionNameSelect-required" class="helper-text job-error" style="display: none;">Морате изабрати научно-истраживачку установу.</span>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="jobPosition" type="text">
                <label class="label-to-activate" for="jobPosition">Назив позиције (радног места) <span class="asterisk">*</span></label>
                <span id="jobPosition-required" class="helper-text job-error" style="display: none;">Морате унети назив радног места.</span>
                <span id="jobPosition-maxlen" class="helper-text job-error" style="display: none;">Назив радног места не сме садржати више од 255 карактера.</span>
            </div>
        </div>

        <div class="modal-footer">
            @if($edit and $projekat->poslata)
            <a href="#!" class="modal-close waves-effect waves-red btn-flat">Затвори</a>
            @else
            <a href="#!" class="modal-close waves-effect waves-red btn-flat">Откажи</a>
            <a href="javascript: saveJob()" class="waves-effect waves-green btn-flat">Сачувај</a>
            @endif
        </div>
    </div>
</div>
<div id="modal-delete-job" class="modal">
    <div class="modal-content">
        <h4>Образовање</h4>
        <br/>
        <input type="hidden" id="deleteJobId"/>
        <p>Да ли сте сигурни да желите да обришете податке о изабраном запослењу?</p>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red btn-flat">Откажи</a>
            <a href="javascript: deleteJob()" class="modal-close waves-effect waves-green btn-flat">Обриши</a>
        </div>
    </div>
</div>