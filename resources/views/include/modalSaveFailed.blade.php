<div id="modal-save-failed" class="modal">
    <div class="modal-content">
        <h4>Снимање неуспешно</h4>
        <p>Подаци о предлогу пројекта, које сте тренутно унели, нису у складу са прописима или се сервер тренутно не одазива.</p>
    </div>
    <div class="modal-footer">
        <a id="close-save-successful-dialog" href="#!" class="modal-close waves-effect waves-green btn-flat">ОК</a>
    </div>
</div>