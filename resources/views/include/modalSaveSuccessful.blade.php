<div id="modal-save-successful" class="modal">
    <div class="modal-content">
        <h4>Снимање успешно</h4>
        <p>Подаци о предлогу пројекта, које сте тренутно унели, успешно су сачувани у бази.<br/>Страница ће се ажурирати након што притиснете ОК.</p>
    </div>
    <div class="modal-footer">
        <a id="close-save-successful-dialog" href="#!" class="modal-close waves-effect waves-green btn-flat">ОК</a>
    </div>
</div>