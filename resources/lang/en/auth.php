<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Корисничко име и/или лозинка не постоје у бази података.',
    'throttle' => 'Превише неуспешних покушаја у кратком временском периоду. Можете покушати поново за :seconds секунди.',
    'blocked' => 'Кориснички налог је блокиран.'
];
